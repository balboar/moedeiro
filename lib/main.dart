import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:dynamic_color/dynamic_color.dart';
import 'package:moedeiro/theme/theme.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/database/database.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/util/routeGenerator.dart';

Future<void> main() async {
//   debugPaintPointersEnabled = true;
//   debugPaintBaselinesEnabled = true;
//   debugPaintLayerBordersEnabled = true;

  SettingsModel model;
  WidgetsFlutterBinding.ensureInitialized();
  await DB.init();
  model = SettingsModel();
  await model.initPrefs();

  runApp(
    ChangeNotifierProvider<SettingsModel>(
      create: (BuildContext context) => model,
      child: MyApp(
        locale: model.locale,
        lockScreen: model.lockScreen,
        useBiometrics: model.useBiometrics,
        pin: model.pin,
        firstTime: model.firstTime,
        themeMode: model.themeMode,
        themeColor: model.themeColor,
        useDefaultThemeColor: model.useDefaultThemeColor,
      ),
    ),
  );
}

class MyApp extends StatefulWidget {
  final Locale? locale;
  final bool lockScreen;
  final bool useBiometrics;
  final String? pin;
  final bool firstTime;
  final ThemeMode themeMode;
  final Color themeColor;
  final bool useDefaultThemeColor;

  MyApp({
    Key? key,
    this.locale,
    required this.lockScreen,
    required this.useBiometrics,
    required this.firstTime,
    required this.themeMode,
    required this.themeColor,
    required this.useDefaultThemeColor,
    this.pin,
  }) : super(key: key);

  static void setLocale(BuildContext context, Locale? newLocale) async {
    _MyAppState? state = context.findAncestorStateOfType<_MyAppState>();
    state!.changeLanguage(newLocale);
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final String theme = 'dark';
  Locale? _locale;
  late final ValueNotifier<ThemeSettings> settings;
  changeLanguage(Locale? locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void initState() {
    super.initState();

    _locale = widget.locale;
    settings = ValueNotifier(ThemeSettings(
      sourceColor: widget.themeColor,
      themeMode: widget.themeMode,
      useDefaultThemeColor: widget.useDefaultThemeColor,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AccountModel>(
          create: (BuildContext context) => AccountModel(),
        ),
        ChangeNotifierProvider<CategoryModel>(
          create: (BuildContext context) => CategoryModel(),
        ),
        ChangeNotifierProvider<TransactionModel>(
          create: (BuildContext context) => TransactionModel(),
        ),
        ChangeNotifierProvider<TransfersModel>(
          create: (BuildContext context) => TransfersModel(),
        ),
        ChangeNotifierProvider<AnalyticsModel>(
          create: (BuildContext context) => AnalyticsModel(),
        ),
        ChangeNotifierProvider<RecurrenceModel>(
          create: (BuildContext context) => RecurrenceModel(),
        ),
      ],
      child: DynamicColorBuilder(
        builder: (lightDynamic, darkDynamic) {
          return ThemeProvider(
              lightDynamic: lightDynamic,
              darkDynamic: darkDynamic,
              settings: settings,
              child: NotificationListener<ThemeSettingChange>(
                onNotification: (notification) {
                  settings.value = notification.settings;
                  return true;
                },
                child: ValueListenableBuilder<ThemeSettings>(
                  valueListenable: settings,
                  builder: (context, value, _) {
                    final theme = ThemeProvider.of(context); // Add this line
                    return MaterialApp(
                      theme: theme.light((settings.value.useDefaultThemeColor &&
                              lightDynamic != null)
                          ? lightDynamic
                          : ColorScheme.fromSeed(
                              seedColor: settings.value.sourceColor,
                            )), // Add this line
                      darkTheme: theme.dark(
                          (settings.value.useDefaultThemeColor &&
                                  darkDynamic != null)
                              ? darkDynamic
                              : ColorScheme.fromSeed(
                                  seedColor: settings.value.sourceColor,
                                  brightness: Brightness.dark)),
                      themeMode: theme.themeMode(),
                      onGenerateRoute: RouteGenerator.generateRoute,
                      initialRoute:
                          Provider.of<SettingsModel>(context, listen: true)
                                  .lockScreen
                              ? '/lockScreen'
                              : widget.firstTime
                                  ? '/welcomePage'
                                  : '/',
                      onGenerateInitialRoutes: (String initialRouteName) {
                        return [
                          RouteGenerator.generateRoute(
                              RouteSettings(name: initialRouteName, arguments: {
                            "pin": widget.pin,
                            "useBiometrics": widget.useBiometrics,
                          })),
                        ];
                      },
                      localizationsDelegates: [
                        S.delegate,
                        GlobalMaterialLocalizations.delegate,
                        GlobalWidgetsLocalizations.delegate,
                        GlobalCupertinoLocalizations.delegate,
                      ],
                      supportedLocales: S.delegate.supportedLocales,
                      locale: _locale,
                    );
                  },
                ),
              ));
        },
      ),
    );
  }
}
