import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:moedeiro/components/dialogs/InfoDialog.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/models/transfer.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/main/components/lastTransactionsList.dart';
import 'package:moedeiro/screens/movements/components/transactionBottomSheet.dart';
import 'package:moedeiro/screens/movements/components/transactionTransferBottomSheetWidget.dart';
import 'package:moedeiro/screens/movements/components/transferBottomSheetWidget.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:quick_actions/quick_actions.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:intl/intl.dart';

class MainPage extends StatefulWidget {
  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage> with WidgetsBindingObserver {
  late bool lockScreen;
  bool? useBiometrics;
  String? pin;
  final QuickActions quickActions = QuickActions();
  ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);

    init();
    super.initState();
  }

  void init() async {
    await quickActions.initialize((String shortcutType) {
      // AppLock.of(context).showLockScreen();

      // if app was not opened, is would you the same bottomsheet twice
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }

      if (shortcutType == 'transaction') {
        showCustomModalBottomSheet(
          isScrollControlled: true,
          context,
          TransactionBottomSheet(
            transaction:
                Transaction(timestamp: DateTime.now().millisecondsSinceEpoch),
          ),
        );
      } else if (shortcutType == 'transfer') {
        showCustomModalBottomSheet(
            context,
            TransferBottomSheet(
                transfer: Transfer(
                    timestamp: DateTime.now().millisecondsSinceEpoch)));
      }
    });
    quickActions.setShortcutItems(<ShortcutItem>[
      // NOTE: This first action icon will only work on iOS.
      // In a real world project keep the same file name for both platforms.
      ShortcutItem(
        type: 'transaction',
        localizedTitle: S.of(context).transaction,
        icon: 'add',
      ),
      ShortcutItem(
        type: 'transfer',
        localizedTitle: S.of(context).transfer,
        icon: 'transfer',
      ),
      // NOTE: This second action icon will only work on Android.
    ]);
    await Provider.of<RecurrenceModel>(context, listen: false)
        .getRecurrences()
        .then((value) {
      var _recurrences =
          Provider.of<RecurrenceModel>(context, listen: false).recurrences;
      var _pendingRecurrences = _recurrences.where((element) {
        return element.nextEvent! <= DateTime.now().millisecondsSinceEpoch;
      }).toList();

      if (_pendingRecurrences.length > 0) {
        _pendingRecurrences.forEach((element) {
          Provider.of<TransactionModel>(context, listen: false)
              .insertTransactiontIntoDb(Transaction(
                  account: element.account,
                  accountName: element.accountName,
                  amount: element.amount,
                  category: element.category,
                  categoryName: element.category,
                  name: element.name,
                  timestamp: element.nextEvent ??
                      DateTime.now().millisecondsSinceEpoch));
          element.nextEvent =
              Provider.of<RecurrenceModel>(context, listen: false)
                  .computeNextEvent(element);
          Provider.of<RecurrenceModel>(context, listen: false)
              .insertRecurrenceIntoDb(element);
        });
      }
    });
    Provider.of<AccountModel>(context, listen: false).getAccounts();
    Provider.of<CategoryModel>(context, listen: false).getCategories();
    Provider.of<TransactionModel>(context, listen: false).getTransactions();
    Provider.of<TransfersModel>(context, listen: false).getTransfers();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      // went to Background
    }
    if (state == AppLifecycleState.resumed) {
      if (Provider.of<SettingsModel>(context, listen: false).lockScreen)
        Navigator.pushReplacementNamed(context, '/lockScreen', arguments: {
          "pin": Provider.of<SettingsModel>(context, listen: false).pin,
          "useBiometrics":
              Provider.of<SettingsModel>(context, listen: false).useBiometrics
        });
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _scrollController.removeListener(() {});
    super.dispose();
  }

  Future<bool?> _showConfirmCloseDialog(context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return MoedeiroConfirmDialog(
          title: S.of(context).warningClosingBottomSheetTitle,
          content: S.of(context).warningClosingBottomSheetSubtitle,
          confirmButtonText: S.of(context).dismiss,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            showCustomModalBottomSheet(
              context, TransactionTransferBottomSheet(),
              isScrollControlled: true,
           
            );
          },
          child: Icon(Icons.add_outlined)),
      body: CustomScrollView(
        controller: _scrollController,
        slivers: [
          SliverAppBar(
            pinned: false,
            expandedHeight: 180,
            collapsedHeight: 80,
            elevation: 0,
            actions: [
              IconButton(
                //  S.of(context).settings,
                onPressed: () {
                  Navigator.pushNamed(context, '/settingsPage',
                      arguments: true);
                },
                icon: Icon(
                  Icons.settings_sharp,
                  // color: Colors.blueGrey,
                ),
                padding: EdgeInsets.only(
                    left: 15.0, right: 10.0, top: 20.0, bottom: 0.0),
              ),
            ],
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.vertical(bottom: Radius.circular(20))),
              ),
              titlePadding: EdgeInsets.only(
                left: 15.0,
                right: 10.0,
                top: 0.0,
                bottom: 10.0,
              ),
              centerTitle: true,
              title: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    '/accountsPage',
                  );
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      S.of(context).balance,
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    Consumer<AccountModel>(builder: (BuildContext context,
                        AccountModel model, Widget? child) {
                      return Text(
                        '${formatCurrency(context, model.totalAmount)}',
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(fontSize: 28),
                      );
                    }),
                  ],
                ),
              ),
            ),
          ),
          SliverFillRemaining(
            hasScrollBody: false,
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Consumer<AccountModel>(builder:
                    (BuildContext context, AccountModel model, Widget? child) {
                  //formatCurrency(context, model.expenses)
                  return MainPageSectionStateless(
                    S.of(context).expensesAndAnalytics,
                    () {
                      Navigator.pushNamed(
                        context,
                        '/analyticsPage',
                      );
                    },
                    analyticsIcon,
                    subtitle:
                        '${S.of(context).expenses} ${DateFormat.MMMM().format(DateTime.now())} ${formatCurrency(context, model.expenses)}',
                  );
                }),
                MainPageSectionStateless(
                  S.of(context).categories,
                  () {
                    Navigator.pushNamed(
                      context,
                      '/categoriesPage',
                    );
                  },
                  Icon(Icons.space_dashboard_rounded, color: Colors.lime),
                ),
                Consumer<RecurrenceModel>(builder: (BuildContext context,
                    RecurrenceModel model, Widget? child) {
                  //formatCurrency(context, model.expenses)

                  return MainPageSectionStateless(
                    S.of(context).recurrences,
                    () {
                      Navigator.pushNamed(
                        context,
                        '/recurrencesPage',
                      );
                    },
                    Icon(Icons.event_repeat_rounded, color: Colors.green),
                    subtitle: model.recurrences.length > 0
                        ? '${model.recurrences.first.categoryName} ${S.of(context).on} ${DateFormat.yMMMEd().format(
                            DateTime.fromMillisecondsSinceEpoch(
                                model.recurrences.first.nextEvent!),
                          )} '
                        : '',
                  );
                }),
                // MainPageSectionStateless(
                //   S.of(context).debts,
                //   () {
                //     Navigator.pushNamed(
                //       context,
                //       '/debtsPage',
                //     );
                //   },
                //   debtsIcon,
                // ),
                MainPageSectionStateless(
                  S.of(context).transactionsTitle,
                  () {
                    Provider.of<AccountModel>(context, listen: false)
                        .setActiveAccountNull();
                    Navigator.pushNamed(context, '/accountTransactionsPage',
                        arguments: true);
                  },
                  Icon(Icons.list_rounded, color: Colors.indigoAccent),
                ),

                SizedBox(
                  height: 10,
                ),
                LastTransactionsWidget(),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
