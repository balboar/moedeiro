import 'package:flutter/material.dart';
import 'package:moedeiro/util/utils.dart';

class ItemCard extends StatelessWidget {
  final IconData icon;
  final String label;
  final Color color;
  final Function onTap;
  ItemCard({
    Key? key,
    required this.icon,
    required this.label,
    required this.color,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Card(
        child: Container(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Icon(icon, color: color),
              SizedBox(
                width: 5,
              ),
              Text(
                label,
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(
                width: 5,
              ),
              forwardIcon,
            ],
          ),
        ),
      ),
    );
  }
}
