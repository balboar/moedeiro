import 'package:flutter/material.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/screens/movements/components/transactionWidgets.dart';

class AnimatedTransactionTile extends StatefulWidget {
  final int index;
  final Transaction transaction;
  AnimatedTransactionTile(this.index, this.transaction, {Key? key})
      : super(key: key);

  @override
  State<AnimatedTransactionTile> createState() =>
      _AnimatedTransactionTileState();
}

class _AnimatedTransactionTileState extends State<AnimatedTransactionTile> {
  bool _animate = false;
  bool _isStart = true;

  @override
  void initState() {
    _isStart
        ? Future.delayed(Duration(milliseconds: widget.index * 50), () {
            setState(() {
              _animate = true;
              _isStart = false;
            });
          })
        : _animate = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 50),
      opacity: _animate ? 1 : 0,
      curve: Curves.easeOut,
      child: Container(
        height: 75,
        child: TransactionTile(widget.transaction),
      ),
    );
  }
}
