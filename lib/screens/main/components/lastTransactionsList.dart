import 'package:flutter/material.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/main/components/animatedTransactionTile.dart';
import 'package:provider/provider.dart';

class LastTransactionsWidget extends StatefulWidget {
  LastTransactionsWidget({Key? key}) : super(key: key);

  @override
  _LastTransactionsWidgetState createState() => _LastTransactionsWidgetState();
}

class _LastTransactionsWidgetState extends State<LastTransactionsWidget>
    with TickerProviderStateMixin {
  bool _isOpen = true;
  Duration _duration = Duration(milliseconds: 250);
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _buildHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            S.of(context).lastMovements,
            style: Theme.of(context).textTheme.titleLarge!.copyWith(
                fontWeight: FontWeight.bold), //TextStyle(fontSize: 20.0),
          ),
        ),
        GestureDetector(
          child: AnimatedSwitcher(
              transitionBuilder: (Widget child, Animation<double> animation) {
                return ScaleTransition(scale: animation, child: child);
              },
              child: Icon(_isOpen
                  ? Icons.expand_less_rounded
                  : Icons.expand_more_rounded),
              duration: _duration),
          onTap: (() {
            setState(() {
              _isOpen = !_isOpen;
            });
          }),
        )
      ],
    );
  }

  Widget _buildItems() {
    return AnimatedSwitcher(
      duration: _duration,
      child: _isOpen
          ? Consumer<TransactionModel>(
              builder: (BuildContext context, TransactionModel model,
                  Widget? child) {
                if (model.transactions.length == 0)
                  return Container(
                    height: 100,
                    child: NoDataWidgetVertical(),
                  );
                else {
                  List<Transaction> _transactionsList = [];

                  _transactionsList = model.transactions.length > 5
                      ? model.transactions.sublist(0, 5)
                      : model.transactions;

                  return Container(
                    child: Column(
                      children: _buildListItems(_transactionsList),
                    ),
                  );
                }
              },
            )
          : SizedBox(
              height: 1,
            ),
    );
  }

  List<Widget> _buildListItems(List<Transaction> _transactionsList) {
    final listItems = <Widget>[];

    for (var i = 0; i < _transactionsList.length; ++i) {
      //  if (i > 0) listItems.add(Divider());
      listItems.add(
        Container(
          height: 75,
          child: AnimatedTransactionTile(i, _transactionsList[i]),
        ),
      );
    }
    return listItems;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Padding(
        padding:
            const EdgeInsets.only(left: 10.0, right: 15, top: 15, bottom: 10.0),
        child: Column(
          children: [_buildHeader(), _buildItems()],
        ),
      ),
    );
  }
}
