import 'package:flutter/material.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/screens/summary/summary.dart';

class SummaryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).analytics),
        centerTitle: true,
        shadowColor: Colors.transparent,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        actions: [
          // IconButton(
          //     tooltip: 'Analytics',
          //     icon: analyticsIcon,
          //     onPressed: () {
          //       Navigator.pushNamed(
          //         context,
          //         '/chartsPage',
          //       );
          //     }),
        ],
      ),
      body: SafeArea(
        child: BottomSummaryViewer(),
      ),
      bottomNavigationBar: FilterBottomBar(),
    );
  }
}
