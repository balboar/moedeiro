import 'package:flutter/material.dart';
import 'package:moedeiro/screens/summary/summary.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class PercentageViewer extends StatelessWidget {
  final Map<String, dynamic> data;
  final double total;
  final String? month;
  final String year;
  PercentageViewer(this.data, this.total, this.month, this.year, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double percent = data['amount'].abs() / total.abs();
    if (percent > 1 || percent < 0) percent = 0;
    return GestureDetector(
        onTap: () {
          showModalBottomSheet(
              enableDrag: true,
              context: context,
              isScrollControlled: true,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              builder: (BuildContext context) {
                // ARREGLAR ESTO
                return TransactionsListBottomSheet(data['uuid'], month, year);
              });
        },
        child: Container(
          // using color so tapping on container works
          color: Colors.transparent,
          margin: const EdgeInsets.only(bottom: 8.0),
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
          height: 50.0,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      data['name'] ?? '',
                      style: Theme.of(context)
                          .textTheme
                          .titleLarge!
                          .copyWith(fontWeight: FontWeight.normal),
                    ),
                    Text(
                      formatCurrency(context, data['amount']),
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: LinearPercentIndicator(
                  lineHeight: 4.0,
                  percent: percent,
                  backgroundColor: Colors.grey.shade200,
                  progressColor:
                      data["type"] == 'E' ? Colors.red : Colors.green,
                ),
              ),
            ],
          ),
        ));
  }
}
