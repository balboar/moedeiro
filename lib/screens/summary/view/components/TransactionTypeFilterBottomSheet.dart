import 'package:flutter/material.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/summary/summary.dart';
import 'package:provider/provider.dart';

class TransactionTypeFilterBottomSheet extends StatefulWidget {
  @override
  State<TransactionTypeFilterBottomSheet> createState() =>
      _TransactionTypeFilterBottomSheetState();
}

class _TransactionTypeFilterBottomSheetState
    extends State<TransactionTypeFilterBottomSheet> {
// N net, I Income, E Expenses
  TransactionTypeFilter _transactionTypeFilter = TransactionTypeFilter.Expense;

  bool _selectedIncome = false;
  bool _selectedExpense = false;
  bool _selectedNetIncome = false;

  Icon _incomeIconNotSelected = Icon(Icons.arrow_forward_rounded);
  Icon _expenseIconNotSelected = Icon(Icons.arrow_back_rounded);
  Icon _netIncomeIconNotSelected = Icon(Icons.compare_arrows_outlined);

  late Icon _incomeIconSelected;
  late Icon _expenseIconSelected;
  late Icon _netIncomeIconSelected;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _transactionTypeFilter = Provider.of<AnalyticsModel>(context, listen: false)
        .activeTransactionTypeFilter;
    _selectedIncome = _transactionTypeFilter == TransactionTypeFilter.Income;
    _selectedExpense = _transactionTypeFilter == TransactionTypeFilter.Expense;
    _selectedNetIncome = _transactionTypeFilter == TransactionTypeFilter.All;
    _incomeIconSelected = Icon(Icons.arrow_forward_rounded,
        color: Theme.of(context).colorScheme.primary);
    _expenseIconSelected = Icon(Icons.arrow_back_rounded,
        color: Theme.of(context).colorScheme.primary);
    _netIncomeIconSelected = Icon(Icons.compare_arrows_outlined,
        color: Theme.of(context).colorScheme.primary);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(
            right: 20.0,
            left: 20,
            top: 30,
            bottom: MediaQuery.of(context).viewInsets.bottom + 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 20),
              child: Text('${S.of(context).groupby}...',
                  style: Theme.of(context).textTheme.titleLarge),
            ),
            ListTile(
                onTap: () {
                  Provider.of<AnalyticsModel>(context, listen: false)
                      .activeTransactionTypeFilter = TransactionTypeFilter.All;
                  Navigator.pop(context);
                },
                leading: _selectedNetIncome
                    ? _netIncomeIconSelected
                    : _netIncomeIconNotSelected,
                title: Text(S.of(context).netIncome)),
            ListTile(
                onTap: () {
                  Provider.of<AnalyticsModel>(context, listen: false)
                          .activeTransactionTypeFilter =
                      TransactionTypeFilter.Income;
                  Navigator.pop(context);
                },
                leading: _selectedIncome
                    ? _incomeIconSelected
                    : _incomeIconNotSelected,
                title: Text(S.of(context).income)),
            ListTile(
                onTap: () {
                  Provider.of<AnalyticsModel>(context, listen: false)
                          .activeTransactionTypeFilter =
                      TransactionTypeFilter.Expense;
                  Navigator.pop(context);
                },
                leading: _selectedExpense
                    ? _expenseIconSelected
                    : _expenseIconNotSelected,
                title: Text(S.of(context).expense)),
          ],
        ),
      ),
    );
  }
}
