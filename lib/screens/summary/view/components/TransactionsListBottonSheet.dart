import 'package:flutter/material.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/movements/components/transactionWidgets.dart';
import 'package:provider/provider.dart';

class TransactionsListBottomSheet extends StatefulWidget {
  final String categoryUuid;
  final String? month;
  final String year;
  const TransactionsListBottomSheet(this.categoryUuid, this.month, this.year,
      {Key? key})
      : super(key: key);

  @override
  State<TransactionsListBottomSheet> createState() =>
      _TransactionsListBottomSheetState();
}

class _TransactionsListBottomSheetState
    extends State<TransactionsListBottomSheet> {
  bool isLoading = true;
  List<Transaction> _transactions = [];
  @override
  void initState() {
    super.initState();
    _loadData();
  }

  void _loadData() async {
    Provider.of<AnalyticsModel>(context, listen: false)
        .getTransactions(widget.categoryUuid,
            month: widget.month, year: widget.year)
        .then((value) {
      _transactions = value;
      isLoading = false;
      print('value');
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
        expand: false,
        maxChildSize: 1,
        initialChildSize: 0.6,
        builder: (BuildContext context, ScrollController scrollController) {
          if (isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (_transactions.isEmpty) {
            return NoDataWidgetVertical();
          } else {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 10.0,
                vertical: 10.0,
              ),
              child: CustomScrollView(
                  controller: scrollController,
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                      child: NavigationPillWidget(),
                    ),
                    SliverFixedExtentList(
                      itemExtent: 80.0,
                      delegate: SliverChildListDelegate(_transactions
                          .map((transaction) => TransactionTile(transaction))
                          .toList()),
                    )
                  ]),
            );
          }
        });
  }
}
