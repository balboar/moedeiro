enum DateFilter { Monthly, Yearly, Custom }

enum TransactionTypeFilter {
  Income('I'),
  Expense('E'),
  All('A');

  final String value;
  const TransactionTypeFilter(this.value);
}
