class ExpansionPanelItem {
  ExpansionPanelItem({
    required this.expandedValue,
    required this.headerValue,
    required this.headerAmount,
  });

  String expandedValue;
  String headerValue;
  double headerAmount;
  bool _isExpanded = false;
  List<String> items = [];

  List<String> _items = ['car', 'gas', 'bills'];

  bool get isExpanded => _isExpanded;

  set isExpanded(bool value) {
    _isExpanded = value;
    if (_isExpanded)
      items = _items;
    else
      _items.clear();
  }
}
