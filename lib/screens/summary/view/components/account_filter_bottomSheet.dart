import 'package:flutter/material.dart';
import 'package:moedeiro/components/buttonBarForBottomSheet.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/accounts/components/account_tile.dart';
import 'package:provider/provider.dart';

class AccountFilterBottomSheet extends StatefulWidget {
  const AccountFilterBottomSheet({Key? key}) : super(key: key);

  @override
  State<AccountFilterBottomSheet> createState() =>
      _AccountFilterBottomSheetState();
}

class _AccountFilterBottomSheetState extends State<AccountFilterBottomSheet> {
  List<String> _seletedAccounts = [];
  List<String> _seletedAccountsName = [];
  @override
  Widget build(BuildContext context) {
    return Consumer<AccountModel>(
        builder: (BuildContext context, AccountModel model, Widget? child) {
      return DraggableScrollableSheet(
        expand: false,
        maxChildSize: 0.9,
        initialChildSize: 0.6,
        builder: (BuildContext context, ScrollController scrollController) {
          return model.accounts.length == 0
              ? Center(
                  child: NoDataWidgetVertical(),
                )
              : Stack(
                  children: [
                    ListView.builder(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        shrinkWrap: true,
                        controller: scrollController,
                        itemCount: model.accounts.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              if (_seletedAccounts
                                  .contains(model.accounts[index].uuid!)) {
                                _seletedAccounts
                                    .remove(model.accounts[index].uuid!);
                              } else {
                                _seletedAccounts
                                    .add(model.accounts[index].uuid!);
                              }

                              if (_seletedAccountsName
                                  .contains(model.accounts[index].name!)) {
                                _seletedAccountsName
                                    .remove(model.accounts[index].name!);
                              } else {
                                _seletedAccountsName
                                    .add(model.accounts[index].name!);
                              }
                              setState(() {});
                            },
                            child: Card(
                              elevation: 0,
                              color: _seletedAccounts
                                      .contains(model.accounts[index].uuid!)
                                  ? Theme.of(context)
                                      .colorScheme
                                      .primaryContainer
                                  : Colors.transparent,
                              child: AccountTile(
                                account: model.accounts[index],
                              ),
                            ),
                          );
                        }),
                    Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: ButtonBarMoedeiro(
                          oneButtonOnly: true,
                          onPressedButton1: () {
                            Provider.of<AnalyticsModel>(context, listen: false)
                                .activeAccount = _seletedAccounts;
                            Navigator.pop(context, _seletedAccountsName);
                          },
                        )),
                  ],
                );
        },
      );
    });
  }
}
