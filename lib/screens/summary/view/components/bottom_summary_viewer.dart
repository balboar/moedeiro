import 'package:flutter/material.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:provider/provider.dart';

import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/summary/summary.dart';
import 'package:moedeiro/screens/summary/view/components/expansion_panel.dart';
import 'package:moedeiro/util/utils.dart';

class BottomSummaryViewer extends StatefulWidget {
  BottomSummaryViewer({Key? key}) : super(key: key);

  @override
  _BottomSummaryViewerState createState() => _BottomSummaryViewerState();
}

class _BottomSummaryViewerState extends State<BottomSummaryViewer> {
  PageController controller = PageController(
    viewportFraction: 0.5,
  );

  final controllerCharts = PageController(viewportFraction: 1);

  @override
  void initState() {
    loadData();
    super.initState();
  }

  void loadData() {
    Provider.of<AnalyticsModel>(context, listen: false)
            .activeTransactionTypeFilter =
        Provider.of<SettingsModel>(context, listen: false)
            .defaultTransactionTypeFilter;
  }

  Widget _mainBody(AnalyticsModel model) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          child: model.transactionsGrouped.length == 0
              ? NoDataWidgetVertical()
              : model.activeDateFilter['GroupByAccount'] == true
                  ? ExpansionListSummary(
                      model: model,
                    )
                  : ListView.builder(
                      reverse: true,
                      itemExtent: 60.0,
                      itemCount: model.transactionsGrouped.length,
                      itemBuilder: (BuildContext context, int index) {
                        return PercentageViewer(
                            model.transactionsGrouped[index],
                            model.totalExpensesAbs,
                            model.currentMonth,
                            model.currentYear);
                      },
                    ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15.0, bottom: 10.0),
          height: 60,
          child: PageView.builder(
            itemBuilder: (BuildContext context, int index) {
              return MonthDetail(
                model.transactionsSummary[index]['amount'],
                model.transactionsSummary[index]['monthofyear'],
                model.transactionsSummary[index]['year'],
              );
            },
            itemCount: model.transactionsSummary.length,
            pageSnapping: true,
            scrollDirection: Axis.horizontal,
            reverse: true,
            onPageChanged: (int index) async {
              model.selectedIndex = index;
            },
            controller: controller,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AnalyticsModel>(
      builder: (BuildContext context, AnalyticsModel model, Widget? child) {
        return model.transactionsSummary.length == 0
            ? Center(child: NoDataWidgetVertical())
            : _mainBody(model);
      },
    );
  }
}

class ExpansionListSummary extends StatefulWidget {
  final AnalyticsModel model;
  ExpansionListSummary({
    Key? key,
    required this.model,
  }) : super(key: key);

  @override
  State<ExpansionListSummary> createState() => _ExpansionListSummaryState();
}

class _ExpansionListSummaryState extends State<ExpansionListSummary> {
  final List<ExpansionPanelItem> _items = [];

  @override
  void initState() {
    _items.clear();

    for (var item in widget.model.transactionsGrouped) {
      _items.add(ExpansionPanelItem(
          expandedValue: '',
          headerValue: item['accountName'],
          headerAmount: item['amount']));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ExpansionPanelList(
        expansionCallback: (int index, bool isExpanded) {
          setState(() {
            _items[index].isExpanded = !isExpanded;
          });
        },
        children: _items.map<ExpansionPanel>((ExpansionPanelItem item) {
          return ExpansionPanel(
            headerBuilder: (BuildContext context, bool isExpanded) {
              return ListTile(
                title: Text(item.headerValue),
                trailing: Text(formatCurrency(context, item.headerAmount)),
              );
            },
            body: Column(
              children: item.items
                  .map((e) => Text(e))
                  .toList(), //PercentageViewer(item, model.totalExpensesAbs,
              // model.currentMonth, model.currentYear),
            ),
            isExpanded: item.isExpanded,
          );
        }).toList(),
      ),
    );
  }
}
