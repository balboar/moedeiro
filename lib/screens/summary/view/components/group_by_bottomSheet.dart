import 'package:flutter/material.dart';
import 'package:moedeiro/components/buttonBarForBottomSheet.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/summary/summary.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class GroupByBottomSheet extends StatefulWidget {
  @override
  State<GroupByBottomSheet> createState() => _GroupByBottomSheetState();
}

class _GroupByBottomSheetState extends State<GroupByBottomSheet> {
// N net, I Income, E Expenses
  TransactionTypeFilter _transactionTypeFilter = TransactionTypeFilter.Expense;

  bool _selectedIncome = false;
  bool _selectedExpense = false;
  bool _selectedNetIncome = false;

  Icon _incomeIcon = Icon(Icons.arrow_forward_rounded);
  Icon _expenseIcon = Icon(Icons.arrow_back_rounded);
  Icon _netIncomeIcon = Icon(Icons.compare_arrows_outlined);

  TextEditingController _dateFromController = TextEditingController();

  TextEditingController _dateToController = TextEditingController();

  Map<String, dynamic> _data = {
    'Filter': DateFilter.Monthly,
    'Date1': null,
    'Date2': null
  };

  bool _selectedMonthly = false;
  bool _selectedYearly = false;
  bool _selectedCustomRange = false;

  Icon _monthlyIconNotSelected = calendarMonthIcon;
  Icon _yearlyIconNotSelected = calendarIcon;

  late Icon _monthlyIconSelected;
  late Icon _yearlyIconSelected;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _transactionTypeFilter = Provider.of<AnalyticsModel>(context, listen: false)
        .activeTransactionTypeFilter;
    _selectedIncome = _transactionTypeFilter == TransactionTypeFilter.Income;
    _selectedExpense = _transactionTypeFilter == TransactionTypeFilter.Expense;
    _selectedNetIncome = _transactionTypeFilter == TransactionTypeFilter.All;
    _monthlyIconSelected = Icon(Icons.calendar_month_rounded,
        color: Theme.of(context).colorScheme.primary);

    _yearlyIconSelected = Icon(Icons.calendar_today_rounded,
        color: Theme.of(context).colorScheme.primary);
  }

  Future<int?> _selectDate(BuildContext context, DateTime _date) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != _date) {
      return Future.value(picked.millisecondsSinceEpoch);
    }
  }

  Widget _buildBox(bool _selected, Icon _icon, String _label, Function onTap) {
    return GestureDetector(
      onTap: () => onTap(),
      child: AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          decoration: BoxDecoration(
            color: _selected
                ? Theme.of(context).colorScheme.primary
                : Colors.transparent,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                width: 2, color: Theme.of(context).colorScheme.primary),
          ),
          child: Padding(
            padding: const EdgeInsets.all(2),
            child: Stack(
              children: [
                Center(child: _icon),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: 35,
                      child: Center(
                        child: Text(
                          _label,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30, bottom: 15),
            child: Text('${S.of(context).groupby}...',
                style: Theme.of(context).textTheme.titleLarge),
          ),
          Divider(
            thickness: 2,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: (MediaQuery.of(context).size.width - 40) / 3,
                child: GridView.count(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    physics: const NeverScrollableScrollPhysics(),
                    crossAxisSpacing: 15,
                    crossAxisCount: 3,
                    children: [
                      _buildBox(
                        _selectedNetIncome,
                        _netIncomeIcon,
                        S.of(context).netIncome,
                        () {
                          setState(() {
                            _selectedNetIncome = !_selectedNetIncome;
                            _selectedExpense = false;
                            _selectedIncome = false;
                          });
                        },
                      ),
                      _buildBox(
                        _selectedIncome,
                        _incomeIcon,
                        S.of(context).incomes,
                        () {
                          setState(() {
                            _selectedIncome = !_selectedIncome;
                            _selectedExpense = false;
                            _selectedNetIncome = false;
                          });
                        },
                      ),
                      _buildBox(
                        _selectedExpense,
                        _expenseIcon,
                        S.of(context).expenses,
                        () {
                          setState(() {
                            _selectedExpense = !_selectedExpense;
                            _selectedNetIncome = false;
                            _selectedIncome = false;
                          });
                        },
                      ),
                    ]),
              ),
              Divider(
                thickness: 2,
              ),
              // SwitchListTile(
              //     secondary: accountIcon,
              //     title: Text(
              //         '${S.of(context).groupby} ${S.of(context).account}'),
              //     value: _groupByAccount,
              //     onChanged: (bool? value) {
              //       if (value != null) {
              //         setState(() {
              //           _groupByAccount = value;
              //           _data['GroupByAccount'] = _groupByAccount;
              //         });
              //       }
              //     }),
              // Divider(
              //   thickness: 2,
              // ),
              ListTile(
                  onTap: () {
                    setState(() {
                      _data['Filter'] = DateFilter.Monthly;
                      _selectedMonthly = !_selectedMonthly;
                      if (_selectedMonthly) _selectedYearly = false;
                      if (_selectedMonthly) {
                        _dateToController.text = '';
                        _dateFromController.text = '';
                      }
                    });
                  },
                  leading: _selectedMonthly
                      ? _monthlyIconSelected
                      : _monthlyIconNotSelected,
                  title: Text(S.of(context).monthly)),
              ListTile(
                  onTap: () {
                    setState(() {
                      _data['Filter'] = DateFilter.Yearly;
                      _selectedYearly = !_selectedYearly;

                      if (_selectedYearly) _selectedMonthly = false;

                      if (_selectedYearly) {
                        _dateToController.text = '';
                        _dateFromController.text = '';
                      }
                    });
                  },
                  leading: _selectedYearly
                      ? _yearlyIconSelected
                      : _yearlyIconNotSelected,
                  title: Text(S.of(context).yearly)),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
                child: Text(
                  S.of(context).customRange,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    flex: 1,
                    child: TextFormField(
                      readOnly: true,
                      controller: _dateFromController,
                      decoration: InputDecoration(
                        enabledBorder: InputBorder.none,
                        prefixIcon: calendarIcon,
                        labelText: S.of(context).from,
                      ),
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        _selectDate(context, DateTime.now()).then((int? value) {
                          if (value != null) {
                            setState(() {
                              _selectedCustomRange = !_selectedCustomRange;

                              if (_selectedCustomRange) {
                                _selectedMonthly = false;
                                _selectedYearly = false;
                              }

                              _data['Filter'] = DateFilter.Custom;
                              _data['Date1'] =
                                  DateTime.fromMillisecondsSinceEpoch(value);
                              _dateFromController.text =
                                  DateFormat.yMd().format(
                                DateTime.fromMillisecondsSinceEpoch(value),
                              );
                              if (_data['Date2'] == null) {
                                _data['Date2'] = _data['Date1'];
                                _dateToController.text =
                                    _dateFromController.text;
                              }
                            });
                          }
                        });
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: TextFormField(
                      readOnly: true,
                      controller: _dateToController,
                      decoration: InputDecoration(
                        enabledBorder: InputBorder.none,
                        prefixIcon: calendarIcon,
                        labelText: S.of(context).to,
                      ),
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        _selectDate(context, DateTime.now()).then((int? value) {
                          if (value != null) {
                            setState(() {
                              _selectedCustomRange = !_selectedCustomRange;
                              if (_selectedCustomRange) {
                                _selectedMonthly = false;
                                _selectedYearly = false;
                              }

                              _data['Filter'] = DateFilter.Custom;
                              _data['Date2'] =
                                  DateTime.fromMillisecondsSinceEpoch(value);
                              _dateToController.text = DateFormat.yMd().format(
                                DateTime.fromMillisecondsSinceEpoch(value),
                              );
                              if (_data['Date1'] == null) {
                                _data['Date1'] = _data['Date2'];
                                _dateFromController.text =
                                    _dateToController.text;
                              }
                            });
                          }
                        });
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 80,
                child: ButtonBarMoedeiro(
                  oneButtonOnly: true,
                  onPressedButton1: () {
                    if (_selectedNetIncome) {
                      Provider.of<AnalyticsModel>(context, listen: false)
                              .activeTransactionTypeFilter =
                          TransactionTypeFilter.All;
                    } else if (_selectedIncome) {
                      Provider.of<AnalyticsModel>(context, listen: false)
                              .activeTransactionTypeFilter =
                          TransactionTypeFilter.Income;
                    } else if (_selectedExpense) {
                      Provider.of<AnalyticsModel>(context, listen: false)
                              .activeTransactionTypeFilter =
                          TransactionTypeFilter.Expense;
                    }
                    Provider.of<SettingsModel>(context, listen: false)
                            .defaultTransactionTypeFilter =
                        Provider.of<AnalyticsModel>(context, listen: false)
                            .activeTransactionTypeFilter;

                    Provider.of<AnalyticsModel>(context, listen: false)
                        .activeDateFilter = _data;
                    Navigator.of(context).pop(true);
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
