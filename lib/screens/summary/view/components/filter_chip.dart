import 'package:flutter/material.dart';

class SummaryInputChip extends StatefulWidget {
  final String label;
  final IconData? icon;
  final bool showDeleteIcon;
  final Function()? onPressed;
  final Function()? onDeleted;
  SummaryInputChip({
    Key? key,
    required this.label,
    required this.context,
    required this.showDeleteIcon,
    this.icon,
    this.onPressed,
    this.onDeleted,
  }) : super(key: key);

  final BuildContext context;

  @override
  State<SummaryInputChip> createState() => _SummaryInputChipState();
}

class _SummaryInputChipState extends State<SummaryInputChip> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 4),
      child: InputChip(
        avatar: Icon(
          widget.icon,
          size: 18,
          color: Theme.of(context).colorScheme.onSurfaceVariant,
        ),
        deleteIcon: widget.showDeleteIcon ? null : Container(),
        label: Text(widget.label),
        onPressed: widget.onPressed,
        onDeleted: widget.showDeleteIcon ? widget.onDeleted : null,
      ),
    );
  }
}
