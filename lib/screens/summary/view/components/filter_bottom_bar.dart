import 'package:flutter/material.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/summary/summary.dart';
import 'package:moedeiro/screens/summary/view/components/group_by_bottomSheet.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class FilterBottomBar extends StatefulWidget {
  @override
  State<FilterBottomBar> createState() => _FilterBottomBarState();
}

class _FilterBottomBarState extends State<FilterBottomBar> {
  String dateText = '';
  String filterText = '';
  String accountFilterText = '';
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Consumer<AnalyticsModel>(
          builder: (BuildContext context, AnalyticsModel model, Widget? child) {
        dateText = '';
        filterText = '';
        if (model.activeAccount.isEmpty) {
          accountFilterText = S.of(context).account;
        }
        if (model.activeDateFilter['Filter'] == DateFilter.Yearly)
          dateText = S.of(context).yearly;
        else if (model.activeDateFilter['Filter'] == DateFilter.Monthly)
          dateText = S.of(context).monthly;
        else if (model.activeDateFilter['Filter'] == DateFilter.Custom)
          dateText =
              '${DateFormat.yMd().format(model.activeDateFilter['Date1'])} - ${DateFormat.yMd().format(model.activeDateFilter['Date2'])}';
        if (model.activeTransactionTypeFilter == TransactionTypeFilter.Income)
          filterText = S.of(context).incomes;
        else if (model.activeTransactionTypeFilter ==
            TransactionTypeFilter.Expense)
          filterText = S.of(context).expense;
        else if (model.activeTransactionTypeFilter == TransactionTypeFilter.All)
          filterText = S.of(context).netIncome;
        return Container(
          height: 50,
          child: ListView(
            scrollDirection: Axis.horizontal,
            primary: true,
            children: <Widget>[
              _buildGroupByChip(context, model),
              //      _buildTransactionTypeFilterChip(context, model),
              // _buildCalendarFilterChip(context, model),
              _buildAccountFilterChip(context, model),
            ],
          ),
        );
      }),
    );
  }

  SummaryInputChip _buildGroupByChip(
      BuildContext context, AnalyticsModel model) {
    var _filterActive = model.activeTransactionTypeFilter !=
            Provider.of<SettingsModel>(context, listen: false)
                .defaultTransactionTypeFilter ||
        model.activeDateFilter['Filter'] != DateFilter.Monthly ||
        model.activeDateFilter['GroupByAccount'] == true;
    return SummaryInputChip(
        context: context,
        label: S.of(context).groupby,
        showDeleteIcon: _filterActive,
        icon: filterListIcon.icon,
        onDeleted: () {
          model.activeTransactionTypeFilter = model.activeTransactionTypeFilter;
          model.activeDateFilter = {
            'Filter': DateFilter.Monthly,
            'Date1': null,
            'Date2': null,
            'GroupByAccount': false,
          };
        },
        onPressed: () {
          showCustomModalBottomSheet(
            context,
            GroupByBottomSheet(),
            isScrollControlled: true,
            enableDrag: true,
          );
        });
  }

  SummaryInputChip _buildAccountFilterChip(
      BuildContext context, AnalyticsModel model) {
    return SummaryInputChip(
      context: context,
      label: accountFilterText,
      showDeleteIcon: model.activeAccount.isNotEmpty,
      icon: accountIcon.icon,
      onDeleted: () {
        model.activeAccount = [];
      },
      onPressed: () {
        showCustomModalBottomSheet(
          context,
          AccountFilterBottomSheet(),
          isScrollControlled: true,
          enableDrag: true,
        ).then((value) {
          if (value is List<String>) {
            if (value.isNotEmpty) {
              accountFilterText = '';
              value.forEach((element) {
                if (accountFilterText.isEmpty)
                  accountFilterText = '$element';
                else
                  accountFilterText = '$accountFilterText, $element';
              });
              setState(() {});
            }
          }
        });
      },
    );
  }
}
