import 'package:flutter/material.dart';
import 'package:moedeiro/components/moedeiroSliverAppBar.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/models/recurrences.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/debts/components/debtBottomSheet.dart';
import 'package:moedeiro/screens/recurrences/components/recurrenceWidgets.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/generated/l10n.dart';

class DebtsPage extends StatefulWidget {
  DebtsPage({Key? key}) : super(key: key);

  @override
  _DebtsPageState createState() => _DebtsPageState();
}

class _DebtsPageState extends State<DebtsPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final controller = PageController(viewportFraction: 1);

  Widget _buildRecurrencesList() {
    return Consumer<RecurrenceModel>(
      builder: (BuildContext context, RecurrenceModel model, Widget? child) {
        if (model.recurrences.length == 0) {
          return SliverToBoxAdapter(
            child: SizedBox(height: 100, child: NoDataWidgetVertical()),
          );
        } else {
          return SliverPadding(
            padding: const EdgeInsets.only(
                left: 10.0, right: 10, top: 0.0, bottom: 10.0),
            sliver: SliverFixedExtentList(
              itemExtent: 120.0,
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return RecurrenceCard(
                    model.recurrences[index],
                    key: Key(model.recurrences[index].uuid!),
                  );
                },
                childCount: model.recurrences.length,
              ),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          MoedeiroSliverAppBar(
            S.of(context).recurrences,
          ),
          _buildRecurrencesList(),
        ],
      ),
      key: _formKey,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showCustomModalBottomSheet(
              context,
              DebtBottomSheet(Recurrence(
                  amount: 0, timestamp: DateTime.now().millisecondsSinceEpoch)),
              enableDrag: false);
        },
        child: Icon(Icons.add_outlined),
      ),
    );
  }
}
