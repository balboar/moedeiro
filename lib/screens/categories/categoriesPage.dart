import 'package:flutter/material.dart';
import 'package:flutter_simple_treeview/flutter_simple_treeview.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/models/categories.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/categories/components/CategoryBottomSheetWidget.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/generated/l10n.dart';

class CategoriesPage extends StatelessWidget {
  final String? data;
  CategoriesPage({this.data});

  Widget buildTabs(BuildContext context) {
    return TabBar(
      tabs: [
        Tab(
          text: S.of(context).expenses,
        ),
        Tab(
          text: S.of(context).incomes,
        ),
      ],
    );
  }

  List<TreeNode> _buildTreeNodeList(
      BuildContext context, List<Categori> _cat, int level, String? parent) {
    var list = _cat.where((element) {
      return element.parent == parent;
    }).toList();
    return list.map((e) {
      return TreeNode(
          content: Container(
            height: 60,
            width: MediaQuery.of(context).size.width - (level * 48),
            child: ListTile(
              title: Text(
                e.name!,
              ),
              onTap: () => {
                if (data == 'newTransaction')
                  Navigator.pop(context, {
                    'uuid': e.uuid,
                    'name': e.name,
                    'type': e.type,
                    'defaultAccount': e.defaultAccount,
                  })
                else
                  Navigator.pushNamed(
                    context,
                    '/categoryPage',
                    arguments: e.uuid,
                  )
              },
            ),
          ),
          children: _buildTreeNodeList(context, _cat, level + 1, e.uuid));
    }).toList();
  }

  Widget buildExpensesTree() {
    return Consumer<CategoryModel>(
        builder: (BuildContext context, CategoryModel model, Widget? child) {
      return buildTree(context, model.expenseCategories);
    });
  }

  Widget buildIncomeTree() {
    return Consumer<CategoryModel>(
        builder: (BuildContext context, CategoryModel model, Widget? child) {
      return buildTree(context, model.incomecategories);
    });
  }

  Widget buildTree(BuildContext context, List<Categori> _categories) {
    return _categories.length == 0
        ? NoDataWidgetVertical()
        : SingleChildScrollView(
            child: TreeView(
              nodes: _buildTreeNodeList(context, _categories, 1, null),
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2, // This is the number of tabs.
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            S.of(context).categoryTitle,
          ),
          bottom: buildTabs(context) as PreferredSizeWidget?,
        ),
        body: TabBarView(
            // These are the contents of the tab views, below the tabs.
            children: <Widget>[
              buildExpensesTree(),
              buildIncomeTree(),
            ]),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add_outlined),
          onPressed: () {
            showCustomModalBottomSheet(
              context,
              CategoryBottomSheet(null, true),
            );
          },
        ),
      ),
    );
  }
}
