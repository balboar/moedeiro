import 'package:flutter/material.dart';
import 'package:moedeiro/components/buttonBarForBottomSheet.dart';
import 'package:moedeiro/components/dialogs/InfoDialog.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/models/categories.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/accounts/components/AccountsListBottonSheet.dart';
import 'package:provider/provider.dart';

import '../../../util/icons.dart';

enum CategoryType { income, expense }

class CategoryBottomSheet extends StatefulWidget {
  final Categori? category;
  final bool emptyCategory;

  CategoryBottomSheet(this.category, this.emptyCategory);
  @override
  State<StatefulWidget> createState() {
    return CategoryBottomSheetState();
  }
}

class CategoryBottomSheetState extends State<CategoryBottomSheet> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _accountController = TextEditingController();
  TextEditingController _parentController = TextEditingController();
  late Categori category;
  CategoryType? _categoryType = CategoryType.income;
  double space = 7.0;

  @override
  void initState() {
    category = widget.category ?? Categori(type: 'E');
    if (category.type == 'E') _categoryType = CategoryType.expense;
    _accountController.text = category.accountName ?? '';
    _parentController.text = category.parentName ?? '';
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    _parentController.dispose();
    _accountController.dispose();
    super.dispose();
  }

  Future<bool?> _showMyDialog() async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return MoedeiroConfirmDialog(
            title: S.of(context).deleteCategory,
            content: S.of(context).deleteCategoryDescription,
            confirmButtonText: S.of(context).delete);
      },
    );
  }

  Future<bool?> _showCategoryNotEmptyDialog() async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return MoedeiroErrorDialog(
          title: S.of(context).deleteCategoryError,
          content: S.of(context).deleteCategorytDescriptionError,
        );
      },
    );
  }

  void deleteCategory() {
    if (!widget.emptyCategory)
      _showCategoryNotEmptyDialog().then((value) => Navigator.pop(context));
    else if (category.uuid != null) {
      _showMyDialog().then((value) {
        if (value!) {
          Provider.of<CategoryModel>(context, listen: false)
              .delete(category.uuid!);
          Navigator.pop(context);
          Navigator.pop(context);
        }
      });
    }
  }

  void _submitForm() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      Provider.of<CategoryModel>(context, listen: false)
          .insertCategoryIntoDb(category);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Padding(
            padding: EdgeInsets.only(
                right: 20.0,
                left: 20,
                top: 5,
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: space,
                ),
                TextFormField(
                  textCapitalization: TextCapitalization.sentences,
                  initialValue: category.name,
                  decoration: InputDecoration(
                    enabledBorder: InputBorder.none,
                    prefixIcon: Icon(Icons.description_rounded),
                    labelText: S.of(context).name,
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return S.of(context).nameError;
                    }
                    return null;
                  },
                  onSaved: (String? value) {
                    category.name = value;
                  },
                ),
                SizedBox(
                  height: space,
                ),
                TextFormField(
                  readOnly: true,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _parentController,
                  decoration: InputDecoration(
                    enabledBorder: InputBorder.none,
                    prefixIcon: Icon(
                      Icons.dashboard_outlined,
                    ),
                    suffixIcon: Visibility(
                      visible: _parentController.text.isNotEmpty,
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            _parentController.clear();
                          });
                          category.parent = null;
                        },
                        icon: clearIcon,
                      ),
                    ),
                    labelText: S.of(context).parentCategory,
                  ),
                  validator: (value) {
                    if ((category.uuid != null) &&
                        (category.parent == category.uuid))
                      return S.of(context).errorParentCategory;
                    else
                      return null;
                  },
                  onTap: () async {
                    FocusScope.of(context).requestFocus(FocusNode());
                    Map<String, dynamic>? result = await Navigator.pushNamed(
                        context, '/categoriesPage',
                        arguments: 'newTransaction');
                    if (result != null) {
                      category.parent = result['uuid'];
                      _parentController.text = result['name'];
                    }
                  },
                ),
                SizedBox(
                  height: space,
                ),
                TextFormField(
                  readOnly: true,
                  controller: _accountController,
                  decoration: InputDecoration(
                    enabledBorder: InputBorder.none,
                    prefixIcon: Icon(Icons.account_balance_wallet),
                    labelText: S.of(context).defaultAccount,
                    suffixIcon: Visibility(
                      visible: _accountController.text.isNotEmpty,
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            _accountController.clear();
                          });
                          category.defaultAccount = null;
                        },
                        icon: clearIcon,
                      ),
                    ),
                  ),
                  validator: (value) {
                    return null;
                  },
                  onTap: () async {
                    FocusScope.of(context).requestFocus(FocusNode());

                    showCustomModalBottomSheet(
                      context,
                      AccountListBottomSheet(),
                      isScrollControlled: true,
                    ).then(
                      (value) {
                        if (value != null) {
                          category.defaultAccount = value;
                          _accountController.text =
                              Provider.of<AccountModel>(context, listen: false)
                                  .getAccountName(value);
                          category.accountName = _accountController.text;
                        }
                      },
                    );
                  },
                ),
                SizedBox(
                  height: space,
                ),
                RadioListTile(
                  title: Text(S.of(context).income),
                  value: CategoryType.income,
                  groupValue: _categoryType,
                  onChanged: (CategoryType? value) {
                    setState(() {
                      _categoryType = value;
                      category.type = 'I';
                    });
                  },
                ),
                RadioListTile(
                  title: Text(S.of(context).expense),
                  value: CategoryType.expense,
                  groupValue: _categoryType,
                  onChanged: (CategoryType? value) {
                    setState(() {
                      _categoryType = value;
                      category.type = 'E';
                    });
                  },
                ),
                ButtonBarMoedeiro(
                  oneButtonOnly: category.uuid == null,
                  onPressedButton1: () {
                    _submitForm();
                  },
                  onPressedButton2: () {
                    deleteCategory();
                  },
                ),
              ],
            )),
      ),
    );
  }
}
