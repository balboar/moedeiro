import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/theme/theme.dart';
import 'package:provider/provider.dart';

import '../../../components/buttons.dart';

class ColorPickerDialog extends StatefulWidget {
  final Color dialogPickerColor;
  ColorPickerDialog(this.dialogPickerColor, {Key? key}) : super(key: key);

  @override
  State<ColorPickerDialog> createState() => _ColorPickerDialogState();
}

class _ColorPickerDialogState extends State<ColorPickerDialog> {
  late Color myColor;
  @override
  void initState() {
    super.initState();
    myColor = widget.dialogPickerColor;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
      content: SingleChildScrollView(
        child: ColorPicker(
          // Use the dialogPickerColor as start color.
          color: myColor,
          // Update the dialogPickerColor using the callback.

          onColorChanged: (Color color) {
            setState(() {
              myColor = color;
            });
            final newSettings = ThemeSettings(
              sourceColor: myColor,
              themeMode:
                  Provider.of<SettingsModel>(context, listen: false).themeMode,
              useDefaultThemeColor: false,
            );
            ThemeSettingChange(settings: newSettings).dispatch(context);
          },
          width: 40,
          height: 40,
          borderRadius: 4,
          spacing: 5,
          runSpacing: 5,
          padding: EdgeInsets.all(8),

          heading: Text(
            S.of(context).selectColor,
            style: Theme.of(context).textTheme.titleMedium,
          ),
          subheading: Text(
            S.of(context).selectColorShade,
            style: Theme.of(context).textTheme.titleMedium,
          ),

          //  showMaterialName: true,
          showColorName: true,
          //  showColorCode: true,

          materialNameTextStyle: Theme.of(context).textTheme.bodySmall,
          colorNameTextStyle: Theme.of(context).textTheme.bodySmall,
          colorCodeTextStyle: Theme.of(context).textTheme.bodySmall,
          pickersEnabled: const <ColorPickerType, bool>{
            ColorPickerType.both: false,
            ColorPickerType.primary: true,
            ColorPickerType.accent: false,
            ColorPickerType.bw: false,
            ColorPickerType.custom: false,
            ColorPickerType.wheel: false,
          },
          // customColorSwatchesAndNames: colorsNameMap,
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(S.of(context).cancel),
          onPressed: () {
            Navigator.of(context).pop(false);
          },
        ),
        TextButton(
          child: Text(S.of(context).acceptButtonText),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ],
    );
  }
}
