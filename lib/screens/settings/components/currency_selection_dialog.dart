import 'package:flutter/material.dart';
import 'package:moedeiro/components/buttons.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/generated/l10n.dart';

class CurrencySelectionDialog extends StatelessWidget {
  const CurrencySelectionDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(S.of(context).currency),
      content: SingleChildScrollView(
        child: RadioButtons(),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            S.of(context).acceptButtonText,
          ),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ],
    );
  }
}

class RadioButtons extends StatefulWidget {
  RadioButtons({Key? key}) : super(key: key);

  @override
  _RadioButtonsState createState() => _RadioButtonsState();
}

class _RadioButtonsState extends State<RadioButtons> {
  String _selectedValue = 'EUR';

  @override
  void initState() {
    getCurrency();
    super.initState();
  }

  void getCurrency() async {
    setState(() {
      _selectedValue = Provider.of<SettingsModel>(context, listen: false)
          .activeCurrency
          .iso4217Code;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListBody(
      children: currencyOptions
          .map(
            (CurrencyOption e) => RadioListTile(
              title: Text(e.displayName),
              value: e.iso4217Code,
              groupValue: _selectedValue,
              onChanged: (dynamic val) async {
                Provider.of<SettingsModel>(context, listen: false)
                    .activeCurrency = e;

                setState(() {
                  _selectedValue = val;
                });
                Navigator.of(context).pop(true);
              },
            ),
          )
          .toList(),
    );
  }
}
