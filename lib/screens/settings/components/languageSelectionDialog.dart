import 'package:flutter/material.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/generated/l10n.dart';

class LanguageSelectionDialog extends StatelessWidget {
  const LanguageSelectionDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(S.of(context).language),
      content: SingleChildScrollView(
        child: RadioButtons(),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            S.of(context).acceptButtonText,
          ),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ],
    );
  }
}

class RadioButtons extends StatefulWidget {
  RadioButtons({Key? key}) : super(key: key);

  @override
  _RadioButtonsState createState() => _RadioButtonsState();
}

class _RadioButtonsState extends State<RadioButtons> {
  String? _selectedValue = 'system';

  @override
  void initState() {
    getLocale();
    super.initState();
  }

  void getLocale() async {
    setState(() {
      _selectedValue =
          Provider.of<SettingsModel>(context, listen: false).localeString;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListBody(
      children: languageOptions
          .map(
            (LanguageOption e) => RadioListTile(
              title: Text(e.languageCode == 'system'
                  ? S.of(context).systemDefaultTitle
                  : e.displayName),
              value: e.languageCode,
              groupValue: _selectedValue,
              onChanged: (dynamic val) async {
                Provider.of<SettingsModel>(context, listen: false)
                    .localeString = val.toString();

                setState(() {
                  _selectedValue = val;
                });
                Navigator.of(context).pop(true);
              },
            ),
          )
          .toList(),
    );
  }
}
