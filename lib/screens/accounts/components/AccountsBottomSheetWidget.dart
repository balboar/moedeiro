import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:moedeiro/components/buttonBarForBottomSheet.dart';
import 'package:moedeiro/components/dialogs/InfoDialog.dart';
import 'package:moedeiro/models/accounts.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/generated/l10n.dart';

class AccountBottomSheet extends StatefulWidget {
  final Account? activeAccount;

  AccountBottomSheet(this.activeAccount);
  @override
  State<StatefulWidget> createState() {
    return _AccountBottomSheetState();
  }
}

class _AccountBottomSheetState extends State<AccountBottomSheet> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late Account activeAccount;
  double _space = 7.0;

  @override
  void initState() {
    activeAccount = widget.activeAccount ?? Account(initialAmount: 0.00);
    checkIcon();
    super.initState();
  }

  void checkIcon() async {
    if (activeAccount.icon != null) {
      var file = await File(activeAccount.icon!).exists();
      if (!file) {
        activeAccount.icon = null;
      }
    }
  }

  Future getImageFromFile() async {
    FilePickerResult? pickedFile = await FilePicker.platform.pickFiles(
      type: FileType.image,
      allowMultiple: false,
    );
    Directory _destination = await getApplicationDocumentsDirectory();
    if (pickedFile != null) {
      String _imagePath = _destination.path +
          '/${activeAccount.uuid}${p.extension(pickedFile.files.single.path!)}';
      print(activeAccount.uuid);
      _destination.listSync().forEach((element) {
        print(element.path);
        print(p.basenameWithoutExtension(element.path));
        if (p.basenameWithoutExtension(element.path) == activeAccount.uuid) {
          element.deleteSync();
        }
      });

      activeAccount.icon = _imagePath;
      setState(() {});
    }
  }

  Future<bool?> _showMyDialog() async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return MoedeiroConfirmDialog(
            title: S.of(context).deleteAccount,
            content: S.of(context).deleteAccountDescription,
            confirmButtonText: S.of(context).delete);
      },
    );
  }

  Future<bool?> _showAccountNotEmptyDialog() async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return MoedeiroErrorDialog(
          title: S.of(context).deleteAccountError,
          content: S.of(context).deleteAccountDescriptionError,
        );
      },
    );
  }

  void deleteAccount() {
    List<Transaction> accountTransactions =
        Provider.of<TransactionModel>(context, listen: false)
            .getAccountTransactions(activeAccount.uuid);
    if (accountTransactions.length > 0)
      _showAccountNotEmptyDialog().then((value) => Navigator.pop(context));
    else if (activeAccount.uuid != null) {
      _showMyDialog().then((value) {
        if (value!) {
          Provider.of<AccountModel>(context, listen: false)
              .deleteAccount(activeAccount.uuid!);
          Navigator.pop(context);
          Navigator.pop(context);
        }
      });
    }
  }

  void _submitForm() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      Provider.of<AccountModel>(context, listen: false)
          .insertAccountIntoDb(activeAccount);
      Navigator.pop(context, true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Padding(
          padding: EdgeInsets.only(
              right: 20.0,
              left: 20.0,
              top: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Column(
            children: <Widget>[
              GestureDetector(
                child: CircleAvatar(
                  backgroundImage: activeAccount.icon != null
                      ? FileImage(
                          File(
                            activeAccount.icon!,
                          ),
                          scale: 0.9)
                      : null,
                  backgroundColor: Theme.of(context).dialogBackgroundColor,
                  child: activeAccount.icon != null
                      ? null
                      : Icon(Icons.add_rounded, size: 45),
                  radius: 40,
                ),
                onTap: getImageFromFile,
              ),
              SizedBox(
                height: _space * 3,
              ),
              TextFormField(
                textCapitalization: TextCapitalization.sentences,
                initialValue: activeAccount.name,
                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  prefixIcon: Icon(Icons.account_balance_wallet),
                  labelStyle: TextStyle(fontSize: 20.0),
                  // icon: Icon(Icons.account_balance_wallet),
                  labelText: S.of(context).accountName,
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return S.of(context).accountName;
                  }
                  return null;
                },
                onSaved: (String? value) {
                  activeAccount.name = value;
                },
              ),
              SizedBox(
                height: _space,
              ),
              TextFormField(
                initialValue: activeAccount.initialAmount.toString(),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  prefixIcon: currencyIcon,
                  labelText: S.of(context).initialAmount,
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return S.of(context).initialAmountError;
                  }
                  return null;
                },
                onSaved: (String? value) {
                  activeAccount.initialAmount = double.parse(value!);
                },
              ),
              ButtonBarMoedeiro(
                oneButtonOnly: activeAccount.uuid == null,
                onPressedButton1: () {
                  _submitForm();
                },
                onPressedButton2: () {
                  deleteAccount();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
