import 'dart:io';
import 'package:flutter/material.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/models/accounts.dart';
import '../../../util/utils.dart';

class AccountTile extends StatelessWidget {
  final Account account;
  final GestureTapCallback? onTap;
  final bool showMonthSummary;
  const AccountTile(
      {required this.account,
      this.onTap,
      this.showMonthSummary = false,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        backgroundImage: account.icon != null
            ? FileImage(
                File(
                  account.icon!,
                ),
                scale: 0.9)
            : null,
        backgroundColor: Theme.of(context).colorScheme.onPrimary,
        child: account.icon != null
            ? null
            : Center(
                child: Text(
                account.name!.substring(0, 1),
                style: Theme.of(context).textTheme.titleLarge,
              )), // Icon(Icons.add_rounded),
        radius: 20,
      ),
      title: Text(
        account.name!, //
        overflow: TextOverflow.clip,
        maxLines: 1,
      ),
      subtitle: Text(
        '${formatCurrency(context, account.amount!)}',
      ),
      trailing: showMonthSummary
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(S.of(context).expensesMonth),
                Text(
                  '${formatCurrency(context, account.expensesMonth!)}',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
              ],
            )
          : null,
      onTap: onTap,
    );
  }
}
