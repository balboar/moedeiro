import 'dart:io';
import 'package:flutter/material.dart';
import 'package:moedeiro/models/accounts.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';

class AccountMiniCard extends StatefulWidget {
  final Account? account;
  AccountMiniCard({Key? key, this.account}) : super(key: key);

  @override
  _AccountMiniCardState createState() => _AccountMiniCardState();
}

class _AccountMiniCardState extends State<AccountMiniCard> {
  @override
  void initState() {
    // checkIcon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            color: Theme.of(context).cardTheme.color,
            child: Padding(
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          widget.account!.name!, //
                          overflow: TextOverflow.clip,
                          maxLines: 1,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color:
                                  Theme.of(context).textTheme.bodyLarge!.color,
                              fontSize: 17.0),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 5.0),
                        child: CircleAvatar(
                          backgroundImage: widget.account!.icon != null
                              ? FileImage(File(widget.account!.icon!),
                                  scale: 0.9)
                              : null,
                          backgroundColor: Colors.transparent,
                          radius: 11,
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  Align(
                    child: Text(
                      '${formatCurrency(context, widget.account!.amount!)}',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    alignment: Alignment.centerLeft,
                  )
                ],
              ),
              padding: EdgeInsets.all(10.0),
            ),
          ),
          onTap: () {
            Provider.of<AccountModel>(context, listen: false).setActiveAccount =
                widget.account!.uuid!;
            Navigator.pushNamed(context, '/accountTransactionsPage',
                arguments: false);
          }),
      width: 170.0,
    );
  }
}

class AccountPageAppBar extends StatefulWidget {
  final Account? activeAccount;
  final List<Widget>? actions;
  final Widget? tabs;

  AccountPageAppBar(this.activeAccount, {this.actions, this.tabs, Key? key})
      : super(key: key);

  @override
  _AccountPageAppBarState createState() => _AccountPageAppBarState();
}

class _AccountPageAppBarState extends State<AccountPageAppBar> {
  double height = 200;
  final controller = PageController(viewportFraction: 1);

  late ScrollController _scrollController;

  bool lastStatus = true;
  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_isShrink != lastStatus) {
      setState(() {
        lastStatus = _isShrink;
      });
    }
  }

  bool get _isShrink {
    return _scrollController.hasClients &&
        _scrollController.offset > (height - kToolbarHeight);
  }

  @override
  Widget build(BuildContext context) {
    return SliverOverlapAbsorber(
      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
      sliver: SliverAppBar(
        bottom: widget.tabs as PreferredSizeWidget?,
        floating: false,
        snap: false,
        pinned: true,
        actions: widget.actions,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          //  expandedTitleScale: 2,
          titlePadding: EdgeInsets.only(bottom: 65),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                widget.activeAccount!.name!,
                style:
                    TextStyle(color: Theme.of(context).colorScheme.onSurface),
              ),
            ],
          ),
        ),
        expandedHeight: MediaQuery.of(context).size.height * 0.30,
      ),
    );
  }
}
