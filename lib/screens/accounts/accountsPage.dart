import 'package:flutter/material.dart';
import 'package:moedeiro/components/moedeiroSliverAppBar.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/accounts/components/AccountsBottomSheetWidget.dart';
import 'package:moedeiro/screens/accounts/components/account_tile.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/generated/l10n.dart';

class AccountsPage extends StatefulWidget {
  AccountsPage({Key? key}) : super(key: key);

  @override
  _AccountsPageState createState() => _AccountsPageState();
}

class _AccountsPageState extends State<AccountsPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final controller = PageController(viewportFraction: 1);

  Widget _buildAccountsList() {
    return Consumer<AccountModel>(
      builder: (BuildContext context, AccountModel model, Widget? child) {
        if (model.accounts.length == 0) {
          return SliverToBoxAdapter(child: NoDataWidgetVertical());
        } else {
          return SliverReorderableList(
              itemBuilder: (BuildContext context, int index) {
                return AccountTile(
                  account: model.accounts[index],
                  onTap: () {
                    Provider.of<AccountModel>(context, listen: false)
                        .setActiveAccount = model.accounts[index].uuid!;
                    Navigator.pushNamed(context, '/accountTransactionsPage',
                        arguments: false);
                  },
                  showMonthSummary: true,
                  key: Key(model.accounts[index].uuid!),
                );
              },
              itemCount: model.accounts.length,
              onReorder: model.reorderAccounts);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          MoedeiroSliverAppBar(
            S.of(context).accountsTitle,
          ),
          _buildAccountsList(),
        ],
      ),
      key: _formKey,
      floatingActionButton: FloatingActionButton.extended(
        label: Text(S.of(context).account),
        onPressed: () {
          showCustomModalBottomSheet(context, AccountBottomSheet(null),
              enableDrag: false);
        },
        icon: Icon(Icons.add_outlined),
      ),
    );
  }
}
