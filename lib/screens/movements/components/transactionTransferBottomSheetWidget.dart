import 'package:flutter/material.dart';
import 'package:moedeiro/components/moedeiroWidgets.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/models/transfer.dart';
import 'package:moedeiro/screens/movements/components/transactionBottomSheet.dart';
import 'package:moedeiro/screens/movements/components/transferBottomSheetWidget.dart';

class TransactionTransferBottomSheet extends StatefulWidget {
  final Transaction? transaction;
  final Transfer? transfer;
  TransactionTransferBottomSheet({this.transaction, this.transfer});
  @override
  State<StatefulWidget> createState() {
    return _TransactionTransferBottomSheetState();
  }
}

class _TransactionTransferBottomSheetState
    extends State<TransactionTransferBottomSheet>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int selectedPage = 0;

  final controller = PageController(
    viewportFraction: 1,
  );
  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: 2,
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    controller.dispose();
    super.dispose();
  }

  void setPage(int page) {
    setState(() {
      selectedPage = page;
    });
  }

  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(
          right: 20.0,
          left: 20,
          top: 10,
          bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Column(
        children: [
          MoedeiroTransactionTransferButtons(setPage),
          IntrinsicHeight(
            child: selectedPage == 0
                ? TransactionDetailsWidget(
                    transaction: widget.transaction ??
                        Transaction(
                            timestamp: DateTime.now().millisecondsSinceEpoch))
                : TransferDetailsWidget(
                    transfer: widget.transfer ??
                        Transfer(
                            timestamp: DateTime.now().millisecondsSinceEpoch)),
          ),
        ],
      ),
    );
  }
}
