import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/screens/movements/components/transactionBottomSheet.dart';
import 'package:moedeiro/util/utils.dart';

class TransactionTile extends StatelessWidget {
  final Transaction transaction;
  final double? accountBalance;
  const TransactionTile(this.transaction, {Key? key, this.accountBalance})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showCustomModalBottomSheet(
          context,
          TransactionBottomSheet(transaction: transaction),
          isScrollControlled: true,
        );
      },
      child: Container(
        margin: EdgeInsets.zero,
        color: Colors.transparent,
        //  elevation: 0,
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              width: 3,
              decoration: BoxDecoration(
                color: transaction.amount! > 0 ? incomeColor : expenseColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(transaction.categoryName ?? '',
                            style: Theme.of(context).textTheme.bodyLarge),
                        Visibility(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            width: 1,
                            height: 15,
                            color: Colors.grey[500],
                          ),
                          visible: transaction.name?.isNotEmpty ?? false,
                        ),
                        Flexible(
                          child: Text(
                            transaction.name ?? '',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: Theme.of(context).textTheme.titleSmall,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                            DateFormat.yMMMEd().format(
                              DateTime.fromMillisecondsSinceEpoch(
                                  transaction.timestamp!),
                            ),
                            style: Theme.of(context).textTheme.bodyMedium),

                        Text(' · ',
                            style: Theme.of(context).textTheme.bodyMedium),
                        Text(transaction.accountName ?? '',
                            style: Theme.of(context).textTheme.bodyMedium),
                        //     Text(
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        formatCurrency(context, transaction.amount!),
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      // Text(
                      //     DateFormat.yMMMEd().format(
                      //       DateTime.fromMillisecondsSinceEpoch(
                      //           transaction.timestamp!),
                      //     ),
                      //     style: Theme.of(context).textTheme.bodyMedium),
                      // Text(
                      //   accountBalance != null
                      //       ? formatCurrency(context, accountBalance)
                      //       : '',
                      //   style: Theme.of(context).textTheme.titleSmall,
                      // ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
