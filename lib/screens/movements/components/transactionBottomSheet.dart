import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moedeiro/components/buttonBarForBottomSheet.dart';
import 'package:moedeiro/components/dialogs/InfoDialog.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/provider/mainModel.dart';
import 'package:moedeiro/screens/accounts/components/AccountsListBottonSheet.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:moedeiro/generated/l10n.dart';

class TransactionBottomSheet extends StatelessWidget {
  final Transaction transaction;
  const TransactionBottomSheet({Key? key, required this.transaction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(
          right: 20.0,
          left: 20,
          bottom: MediaQuery.of(context).viewInsets.bottom),
      child: IntrinsicHeight(
          child: TransactionDetailsWidget(
        transaction: transaction,
      )),
    );
  }
}

class TransactionDetailsWidget extends StatefulWidget {
  final Transaction transaction;
  TransactionDetailsWidget({required this.transaction});
  @override
  State<StatefulWidget> createState() {
    return _TransactionDetailsWidgetState();
  }
}

class _TransactionDetailsWidgetState extends State<TransactionDetailsWidget> {
  late Transaction _originalTransaction;
  late Transaction _transaction;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();
  TextEditingController _categoryController = TextEditingController();
  TextEditingController _accountController = TextEditingController();
  TextEditingController _amountController = TextEditingController();
  bool? isExpense;
  double space = 7.0;

  @override
  void initState() {
    _originalTransaction = widget.transaction.copyWith();
    _transaction = widget.transaction.copyWith();
    _dateController.text = _transaction.timestamp != null
        ? DateFormat.yMMMd().format(
            DateTime.fromMillisecondsSinceEpoch(_transaction.timestamp!),
          )
        : '';

    _timeController.text = _transaction.timestamp != null
        ? DateFormat.Hm().format(
            DateTime.fromMillisecondsSinceEpoch(_transaction.timestamp!),
          )
        : '';

    _categoryController.text = _transaction.categoryName ?? '';
    _accountController.text = _transaction.accountName ?? '';
    _amountController.text = _transaction.amount.toString();
    if (_transaction.category != null && isExpense == null)
      isExpense = Provider.of<CategoryModel>(context, listen: false)
          .isExpense(_transaction.category!);

    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    _dateController.dispose();
    _timeController.dispose();
    _categoryController.dispose();
    _accountController.dispose();
    super.dispose();
  }

  // ignore: missing_return
  Future<int?> _selectDate(BuildContext context) async {
    DateTime _date = DateTime.fromMillisecondsSinceEpoch(
        _transaction.timestamp ?? DateTime.now().millisecondsSinceEpoch);
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != _date) {
      return Future.value(picked.millisecondsSinceEpoch);
    }
  }

  // ignore: missing_return
  Future<int?> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(
        DateTime.fromMillisecondsSinceEpoch(
            _transaction.timestamp ?? DateTime.now().millisecondsSinceEpoch),
      ),
    );

    if (picked != null) {
      setState(() {
        DateTime _fecha = DateTime.fromMillisecondsSinceEpoch(
            _transaction.timestamp ?? DateTime.now().millisecondsSinceEpoch);
        int fecha = DateTime(_fecha.year, _fecha.month, _fecha.day, picked.hour,
                picked.minute)
            .millisecondsSinceEpoch;
        _transaction.timestamp = fecha;
      });
      return Future.value(_transaction.timestamp);
    }
  }

  Future<bool?> _showMyDialog() async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return MoedeiroConfirmDialog(
            title: S.of(context).deleteMovement,
            content: S.of(context).deleteMovementDescription,
            confirmButtonText: S.of(context).delete);
      },
    );
  }

  void deleteTransaction() {
    if (_transaction.uuid != null) {
      _showMyDialog().then(
        (value) {
          if (value!) {
            Provider.of<TransactionModel>(context, listen: false)
                .delete(_transaction.uuid!)
                .then(
              (value) {
                Provider.of<AccountModel>(context, listen: false).getAccounts();
                Navigator.pop(context);
              },
            );
          }
        },
      );
    }
  }

  void _submitForm() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      if (isExpense!) _transaction.amount = -1 * _transaction.amount!.abs();
      Provider.of<TransactionModel>(context, listen: false)
          .insertTransactiontIntoDb(_transaction)
          .then(
        (value) {
          Provider.of<AccountModel>(context, listen: false).getAccounts();
          Navigator.pop(context);
        },
      );
    }
  }

  Future<bool?> _showConfirmCloseDialog(context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return MoedeiroConfirmDialog(
          title: S.of(context).warningClosingBottomSheetTitle,
          content: S.of(context).warningClosingBottomSheetSubtitle,
          confirmButtonText: S.of(context).dismiss,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (bool didPop) async {
        bool canPop = false;
        if (didPop) {
          return;
        }
        final NavigatorState navigator = Navigator.of(context);
        _formKey.currentState!.save();
        if (_transaction == _originalTransaction) {
          navigator.pop();
        } else {
          canPop = await _showConfirmCloseDialog(context) ?? false;
          if (canPop) navigator.pop();
        }

        return;
      },
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              textCapitalization: TextCapitalization.sentences,
              initialValue: _transaction.name,
              decoration: InputDecoration(
                enabledBorder: InputBorder.none,
                prefixIcon: Icon(Icons.description_rounded),
                labelText: S.of(context).description,
              ),
              onSaved: (String? value) {
                if (value != null && (value.isNotEmpty))
                  _transaction.name = value;
              },
            ),
            SizedBox(
              height: space,
            ),
            TextFormField(
              controller: _amountController,
              onTap: () {
                if ((_amountController.text != '') &&
                    (double.tryParse(_amountController.text)!.round() == 0)) {
                  _amountController.text = '';
                }
              },
              validator: (value) {
                if (value != null) {
                  value = value.replaceAll(',', '.');
                  if (value.isEmpty) {
                    return S.of(context).amountError;
                  }
                  return null;
                } else {
                  return null;
                }
              },
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                enabledBorder: InputBorder.none,
                prefixIcon: currencyIcon,
                labelText: S.of(context).amount,
              ),
              onSaved: (String? value) {
                if (value != null) {
                  value = value.replaceAll(',', '.');
                  _transaction.amount = double.parse(value);
                }
              },
            ),
            SizedBox(
              height: space,
            ),
            TextFormField(
              readOnly: true,
              controller: _categoryController,
              decoration: InputDecoration(
                enabledBorder: InputBorder.none,
                prefixIcon: Icon(
                  Icons.dashboard_outlined,
                ),
                labelText: S.of(context).categoryTitle,
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return S.of(context).categoryError;
                }
                return null;
              },
              onTap: () async {
                FocusScope.of(context).requestFocus(FocusNode());
                Map<String, dynamic>? result = await Navigator.pushNamed(
                    context, '/categoriesPage',
                    arguments: 'newTransaction');
                if (result != null) {
                  _transaction.category = result['uuid'];
                  _categoryController.text = result['name'];
                  _transaction.categoryName = result['name'];
                  if (result['defaultAccount'] != null) {
                    _transaction.account = result['defaultAccount'];
                    _accountController.text =
                        Provider.of<AccountModel>(context, listen: false)
                            .getAccountName(result['defaultAccount']);
                    _transaction.accountName = _accountController.text;
                  }
                  isExpense = result['type'] == 'E';
                }
              },
            ),
            SizedBox(
              height: space,
            ),
            TextFormField(
              readOnly: true,
              controller: _accountController,
              decoration: InputDecoration(
                enabledBorder: InputBorder.none,
                prefixIcon: Icon(Icons.account_balance_wallet),
                labelText: S.of(context).account,
              ),
              validator: (value) {
                if (_accountController.text.isEmpty) {
                  return S.of(context).accountSelectError;
                }
                return null;
              },
              onTap: () async {
                FocusScope.of(context).requestFocus(FocusNode());

                showCustomModalBottomSheet(
                  context,
                  AccountListBottomSheet(),
                  isScrollControlled: true,
                ).then(
                  (value) {
                    if (value != null) {
                      _transaction.account = value;
                      _accountController.text =
                          Provider.of<AccountModel>(context, listen: false)
                              .getAccountName(value);
                      _transaction.accountName = _accountController.text;
                    }
                  },
                );
              },
            ),
            SizedBox(
              height: space,
            ),
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                    readOnly: true,
                    controller: _dateController,
                    decoration: InputDecoration(
                      enabledBorder: InputBorder.none,
                      prefixIcon: Icon(Icons.calendar_today),
                      labelText: S.of(context).date,
                    ),
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      _selectDate(context).then((int? value) {
                        if (value != null)
                          setState(() {
                            _dateController.text = DateFormat.yMMMd().format(
                              DateTime.fromMillisecondsSinceEpoch(value),
                            );
                            _transaction.timestamp = value;
                          });
                      });
                    },
                  ),
                ),
                SizedBox(
                  width: space,
                ),
                Container(
                  child: TextFormField(
                    readOnly: true,
                    controller: _timeController,
                    decoration: InputDecoration(
                      enabledBorder: InputBorder.none,
                      prefixIcon: Icon(Icons.access_time),
                      labelText: S.of(context).time,
                    ),
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      _selectTime(context).then((int? value) {
                        if (value != null)
                          setState(() {
                            _timeController.text = DateFormat.Hm().format(
                              DateTime.fromMillisecondsSinceEpoch(value),
                            );
                            _transaction.timestamp = value;
                          });
                      });
                    },
                  ),
                  width: 130,
                ),
              ],
            ),
            ButtonBarMoedeiro(
              oneButtonOnly: _transaction.uuid == null,
              onPressedButton1: () {
                _submitForm();
              },
              onPressedButton2: () {
                deleteTransaction();
              },
            ),
          ],
        ),
      ),
    );
  }
}
