import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/models/transaction.dart';
import 'package:moedeiro/screens/movements/components/transactionBottomSheet.dart';
import 'package:moedeiro/util/utils.dart';

class TransactionTileNena extends StatelessWidget {
  final Transaction transaction;
  final double? accountBalance;
  const TransactionTileNena(this.transaction, {Key? key, this.accountBalance})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showCustomModalBottomSheet(
          context,
          TransactionBottomSheet(transaction: transaction),
          isScrollControlled: true,
        );
      },
      child: Card(
        margin: EdgeInsets.zero,
        color: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        child: Row(
          children: [
            CircleAvatar(
              backgroundImage: transaction.accountName!.substring(0, 1) == 'O'
                  ? FileImage(
                      File(
                        '/data/user/0/com.balboar.moedeiro/app_flutter/eec34194-b914-4195-b1fc-2b7af5bdd5cc.jpg',
                      ),
                      scale: 0.9)
                  : null,
              backgroundColor: Theme.of(context).colorScheme.onPrimary,
              child: transaction.accountName!.substring(0, 1) == 'O'
                  ? null
                  : Center(
                      child: Text(
                      transaction.accountName!.substring(0, 1),
                      style: Theme.of(context).textTheme.titleLarge,
                    )), // Icon(Icons.add_rounded),
              radius: 20,
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(transaction.categoryName ?? '',
                            style: Theme.of(context).textTheme.bodyLarge),
                        Visibility(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            width: 1,
                            height: 15,
                            color: Colors.grey[500],
                          ),
                          visible: transaction.name!.isNotEmpty,
                        ),
                        // Flexible(
                        //   child: Text(
                        //     transaction.name ?? '',
                        //     overflow: TextOverflow.ellipsis,
                        //     maxLines: 1,
                        //     style: Theme.of(context).textTheme.titleSmall,
                        //   ),
                        // ),
                      ],
                    ),
                    Row(
                      children: [
                        Flexible(
                          child: Text(
                            transaction.name ?? '',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: Theme.of(context).textTheme.titleSmall,
                          ),
                        ),
                        // Icon(
                        //   Icons.account_balance_wallet_outlined,
                        //   size: 16,
                        // ),
                        // Container(
                        //   width: 10,
                        // ),
                        // Text(transaction.accountName ?? '',
                        //     style: Theme.of(context).textTheme.bodyMedium),
                        //     Text(
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        formatCurrency(context, transaction.amount!),
                        style:
                            // Theme.of(context).textTheme.bodyLarge.copyWith(

                            //       color: transaction.amount > 0
                            //           ? Colors.green.withOpacity(0.9)
                            //           : Colors.red.withOpacity(0.9),
                            //     ),
                            Theme.of(context).textTheme.bodyLarge,
                      ),
                      Text(
                          DateFormat.yMd().format(
                            DateTime.fromMillisecondsSinceEpoch(
                                transaction.timestamp!),
                          ),
                          style: Theme.of(context).textTheme.bodyMedium),
                      // Text(
                      //   accountBalance != null
                      //       ? formatCurrency(context, accountBalance)
                      //       : '',
                      //   style: Theme.of(context).textTheme.titleSmall,
                      // ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10.0,
              ),
              width: 2.5,
              decoration: BoxDecoration(
                color: transaction.amount! > 0 ? Colors.green : Colors.red,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(2),
                  topLeft: Radius.circular(2),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
