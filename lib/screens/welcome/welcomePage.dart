import 'package:cool_stepper/cool_stepper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moedeiro/components/buttons.dart';
import 'package:moedeiro/components/showBottomSheet.dart';
import 'package:moedeiro/database/database.dart';
import 'package:moedeiro/generated/l10n.dart';
import 'package:moedeiro/main.dart';
import 'package:moedeiro/models/settings.dart';
import 'package:moedeiro/screens/accounts/components/AccountsBottomSheetWidget.dart';
import 'package:moedeiro/screens/settings/components/languageSelectionDialog.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:provider/provider.dart';

import '../settings/components/currency_selection_dialog.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({
    Key? key,
  }) : super(key: key);

  final String? title = 'aaaa';

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  String? selectedRole = 'Writer';
  String _errorText = '';
  bool _accountCreated = false;

  String _localeLabel = '';
  String _currencyLabel = '';

  String _localeString = '';

  @override
  void initState() {
    _localeString = 'system';
    super.initState();
  }

  Future<bool?> _showLanguageDialog() async {
    return showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return LanguageSelectionDialog();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Theme.of(context).colorScheme.background));
    _localeLabel = getLocaleLabel(context, _localeString);
    final steps = [
      CoolStep(
        title: '',
        isHeaderEnabled: false,
        subtitle: '',
        content: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Text(
              S.of(context).allSet,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displaySmall,
            ),
            SizedBox(
              height: 40,
            ),
            Text(
              S.of(context).createAccountText,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            SizedBox(
              height: 100,
            ),
            Center(
              child: MainButtonMoedeiro(
                label: _accountCreated
                    ? S.of(context).createAccountSuccess
                    : S.of(context).createAccountText,
                onPressed: () {
                  showCustomModalBottomSheet(context, AccountBottomSheet(null),
                          enableDrag: false)
                      .then((value) {
                    _accountCreated = value ?? false;
                    if (!_accountCreated)
                      setState(() {
                        _errorText = S.of(context).createAccountError;
                      });
                    else
                      setState(() {
                        _errorText = '';
                      });
                  });
                },
              ),
            ),
          ],
        ),
        validation: () {
          if (!_accountCreated) {
            setState(() {
              _errorText = S.of(context).createAccountError;
            });
            return _errorText;
          } else {
            return null;
          }
        },
      ),
      CoolStep(
        title: '',
        isHeaderEnabled: false,
        subtitle: '',
        content: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Center(
                child: Icon(
              Icons.language,
              size: 40,
            )),
            Center(
              child: Text(
                S.of(context).selectLanguage,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            SizedBox(
              height: 100,
            ),
            Center(
              child: MainButtonMoedeiro(
                label: S.of(context).language,
                onPressed: () async {
                  await _showLanguageDialog();
                  _localeString =
                      Provider.of<SettingsModel>(context, listen: false)
                          .localeString;
                  if (_localeString == 'system')
                    MyApp.setLocale(context, Localizations.localeOf(context));
                  else
                    MyApp.setLocale(context,
                        Locale.fromSubtags(languageCode: _localeString));
                  setState(() {
                    _localeLabel = getLocaleLabel(context, _localeString);
                  });
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                _localeLabel,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            ),
          ],
        ),
        validation: () {
          return null;
        },
      ),
      CoolStep(
        title: '',
        isHeaderEnabled: false,
        subtitle: '',
        content: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Center(
                child: Icon(
              Icons.attach_money_rounded,
              size: 40,
            )),
            Center(
              child: Text(
                S.of(context).selectCurrency,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            SizedBox(
              height: 100,
            ),
            Center(
              child: MainButtonMoedeiro(
                label: S.of(context).currency,
                onPressed: () async {
                  showDialog<bool>(
                    context: context,
                    builder: (BuildContext context) {
                      return CurrencySelectionDialog();
                    },
                  ).then((value) {
                    if (value! && value) {
                      setState(() {
                        _currencyLabel =
                            Provider.of<SettingsModel>(context, listen: false)
                                .activeCurrency
                                .displayName;
                      });
                    }
                  });
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                _currencyLabel,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            ),
          ],
        ),
        validation: () {
          return null;
        },
      ),
      CoolStep(
        title: '',
        subtitle: '',
        isHeaderEnabled: false,
        content: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 150,
              ),
              Center(
                child: Text(
                  '👋',
                  style: TextStyle(fontSize: 50),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Center(
                child: Text(
                  S.of(context).allSet,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displaySmall,
                ),
              ),
            ],
          ),
        ),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      showErrorSnackbar: true,
      onCompleted: () {
        if (_localeString == 'system') {
          Locale myLocale = Localizations.localeOf(context);
          _localeString = myLocale.languageCode;
        }
        switch (_localeString) {
          case 'en':
            DB.createCategoriesEnglish();
            break;
          case 'es':
            DB.createCategoriesSpanish();
            break;
          case 'pt':
            DB.createCategoriesPortuguese();
            break;
          default:
            DB.createCategoriesEnglish();
        }
        Provider.of<SettingsModel>(context, listen: false).firstTime = false;
        Navigator.pushReplacementNamed(
          context,
          '/',
        );
      },
      steps: steps,
      config: CoolStepperConfig(
        finalText: S.of(context).finishText,
        stepText: S.of(context).step,
        ofText: S.of(context).ofText,
        nextText: S.of(context).next,
        backText: S.of(context).prev,
      ),
    );

    return Scaffold(
      body: SafeArea(
        child: stepper,
      ),
    );
  }
}
