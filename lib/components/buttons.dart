import 'package:flutter/material.dart';
import 'package:moedeiro/generated/l10n.dart';

class MainButtonMoedeiro extends StatelessWidget {
  final Function? onPressed;
  final String? label;
  const MainButtonMoedeiro({Key? key, this.label, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FilledButton(
      child: Text(
        label ?? S.of(context).save,
      ),
      onPressed: onPressed as void Function()?,
    );
  }
}

class SecondaryButtonMoedeiro extends StatelessWidget {
  final Function? onPressed;
  final String? label;
  const SecondaryButtonMoedeiro({Key? key, this.label, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      child: Text(
        label ?? S.of(context).delete,
      ),
      onPressed: onPressed as void Function()?,
    );
  }
}
