import 'package:flutter/material.dart';
import 'package:moedeiro/generated/l10n.dart';

import '../util/utils.dart';

class NoDataWidgetVertical extends StatelessWidget {
  const NoDataWidgetVertical({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 0.75,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.bar_chart_rounded),
          Text(
            S.of(context).noDataLabel,
            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}

class OptionsCard extends StatelessWidget {
  final IconData icon;
  final MaterialColor color;
  final Function onTap;
  final String label;
  const OptionsCard(this.icon, this.color, this.onTap, this.label, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: Card(
          shape: RoundedRectangleBorder(
            //   side: BorderSide(color: Colors.grey, width: 0.5),
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Icon(
            icon,
            size: 40.0,
          ),
        ),
        width: 85,
      ),
      onTap: onTap as void Function()?,
    );
  }
}

class MainPageSectionStateless extends StatelessWidget {
  final Function onTap;
  final String title;
  final String? subtitle;
  final EdgeInsetsGeometry? padding;
  final Icon icon;

  const MainPageSectionStateless(this.title, this.onTap, this.icon,
      {this.subtitle, this.padding, Key? key})
      : super(key: key);

  Widget _buildTitle(BuildContext context) {
    return Text(
      title,
      style: Theme.of(context)
          .textTheme
          .titleLarge!
          .copyWith(fontWeight: FontWeight.normal), //TextStyle(fontSize: 20.0),
    );
  }

  Widget _buildSubtitleTitle(BuildContext context) {
    return Text(
      subtitle ?? '',
      style:
          Theme.of(context).textTheme.bodyMedium, //TextStyle(fontSize: 20.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap as void Function()?,
      //   child: Card(
      //   margin: EdgeInsets.zero,
      //  color: Colors.,

      child: Container(
        height: 60,
        margin: EdgeInsets.only(left: 10.0, right: 15.0, top: 0.0, bottom: 0.0),
        //  padding: padding ?? const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            icon,
            SizedBox(
              width: 10,
            ),
            subtitle != null && subtitle!.isNotEmpty
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildTitle(context),
                      _buildSubtitleTitle(context)
                    ],
                  )
                : _buildTitle(context),
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: forwardIcon,
              ),
            )
          ],
        ),
        // ),
      ),
    );
  }
}

class NoDataWidgetHorizontal extends StatelessWidget {
  const NoDataWidgetHorizontal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.bar_chart_rounded),
        SizedBox(
          width: 20.0,
        ),
        Text(
          S.of(context).noDataLabel,
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}

class NavigationPillWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
          Container(
              child: Center(
                  child: Wrap(children: <Widget>[
            Container(
                width: 40,
                margin: EdgeInsets.only(top: 10, bottom: 10),
                height: 3,
                decoration: new BoxDecoration(
                  color: Theme.of(context).colorScheme.secondary,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                )),
          ]))),
        ]));
  }
}

class MoedeiroTransactionTransferButtons extends StatefulWidget {
  final Function(int page) tabController;
  MoedeiroTransactionTransferButtons(
    this.tabController, {
    Key? key,
  }) : super(key: key);

  @override
  _MoedeiroTransactionTransferButtonsState createState() =>
      _MoedeiroTransactionTransferButtonsState();
}

const double width = 300.0;
const double height = 40.0;
const double borderRadius = 10.0;
const double transactionAlign = -1;
const double transferAlign = 1;

class _MoedeiroTransactionTransferButtonsState
    extends State<MoedeiroTransactionTransferButtons> {
  late double xAlign;
  Color? transactionColor;
  Color? transferColor;

  Color selectedColor = Colors.black;
  Color normalColor = Colors.black;

  @override
  void initState() {
    xAlign = transactionAlign;

    // widget.tabController.addListener(() {
    //   setState(() {
    //     xAlign = -1 + widget.tabController.page! * 2;

    //     if (widget.tabController.page! < 0.5) {
    //       transactionColor = selectedColor;
    //       transferColor = normalColor;
    //     } else if (widget.tabController.page! > 0.5) {
    //       transferColor = selectedColor;
    //       transactionColor = normalColor;
    //     }
    //   });
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    selectedColor = Theme.of(context).colorScheme.onPrimary;
    normalColor = Theme.of(context).colorScheme.primary;
    transferColor = transferColor ?? normalColor;
    transactionColor = transactionColor ?? selectedColor;

    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Theme.of(context).hoverColor,
        borderRadius: BorderRadius.all(
          Radius.circular(borderRadius),
        ),
      ),
      child: Stack(
        children: [
          AnimatedAlign(
            alignment: Alignment(xAlign, 0),
            duration: Duration(milliseconds: 1),
            child: Container(
              width: width * 0.5,
              height: height,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              widget.tabController(0);

              setState(() {
                xAlign = transactionAlign;

                transferColor = normalColor;
                transactionColor = selectedColor;
              });
            },
            child: Align(
              alignment: Alignment(-1, 0),
              child: Container(
                width: width * 0.5,
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Text(
                  S.of(context).transaction,
                  style: TextStyle(
                    color: transactionColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              widget.tabController(1);

              setState(() {
                xAlign = transferAlign;

                transactionColor = normalColor;
                transferColor = selectedColor;
              });
            },
            child: Align(
              alignment: Alignment(1, 0),
              child: Container(
                width: width * 0.5,
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Text(
                  S.of(context).transfer,
                  style: TextStyle(
                    color: transferColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
