import 'package:flutter/material.dart';
import 'package:moedeiro/generated/l10n.dart';

class MoedeiroErrorDialog extends StatelessWidget {
  final String title;
  final String content;

  const MoedeiroErrorDialog(
      {super.key, required this.title, required this.content});
  @override
  Widget build(BuildContext context) {
    return _MoedeiroDialog(
      icon: Icons.error_outline,
      title: title,
      content: content,
    );
  }
}

class MoedeiroConfirmDialog extends StatelessWidget {
  final String title;
  final String content;
  final String confirmButtonText;

  const MoedeiroConfirmDialog(
      {super.key,
      required this.title,
      required this.content,
      required this.confirmButtonText});
  @override
  Widget build(BuildContext context) {
    return _MoedeiroDialog(
      icon: Icons.warning_rounded,
      title: title,
      content: content,
      confirmButtonText: confirmButtonText,
    );
  }
}

class _MoedeiroDialog extends StatelessWidget {
  final IconData? icon;
  final String title;
  final String content;
  final String? confirmButtonText;
  const _MoedeiroDialog(
      {Key? key,
      this.icon,
      required this.title,
      required this.content,
      this.confirmButtonText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      icon: Icon(icon),
      title: Text(
        title,
      ),
      content: Text(content),
      actions: <Widget>[
        TextButton(
            child: Text(S.of(context).cancel),
            onPressed: () {
              Navigator.of(context).pop(false);
            }),
        Visibility(
            child: TextButton(
                child: Text(confirmButtonText ?? ''),
                onPressed: () {
                  Navigator.of(context).pop(true);
                }),
            visible: confirmButtonText != null),
      ],
    );
  }
}
