// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pt locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pt';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "acceptButtonText": MessageLookupByLibrary.simpleMessage("Aceitar"),
        "account": MessageLookupByLibrary.simpleMessage("Conta"),
        "accountName": MessageLookupByLibrary.simpleMessage("Nome da conta"),
        "accountSelectError":
            MessageLookupByLibrary.simpleMessage("Seleccione uma conta"),
        "accountsTitle": MessageLookupByLibrary.simpleMessage("Contas"),
        "allSet":
            MessageLookupByLibrary.simpleMessage("Bem-vindo ao  Moedeiro"),
        "amount": MessageLookupByLibrary.simpleMessage("Montante"),
        "amountError": MessageLookupByLibrary.simpleMessage(
            "Montante deve ser superior a 0"),
        "analytics": MessageLookupByLibrary.simpleMessage("Resumo"),
        "appTitle": MessageLookupByLibrary.simpleMessage("Moedeiro"),
        "authenticateLabel": MessageLookupByLibrary.simpleMessage(
            "Faça login para ver os dados"),
        "balance": MessageLookupByLibrary.simpleMessage("Saldo"),
        "budgetsTitle": MessageLookupByLibrary.simpleMessage("Orçamentos"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancelar"),
        "categories": MessageLookupByLibrary.simpleMessage("Categorias"),
        "categoryError":
            MessageLookupByLibrary.simpleMessage("Selecciona uma categoria"),
        "categoryTitle": MessageLookupByLibrary.simpleMessage("Categoria"),
        "changePassword": MessageLookupByLibrary.simpleMessage("Mudar senha"),
        "common": MessageLookupByLibrary.simpleMessage("Geral"),
        "confirmPin": MessageLookupByLibrary.simpleMessage("Confirme o PIN"),
        "createAccountError": MessageLookupByLibrary.simpleMessage(
            "Você precisa criar uma conta"),
        "createAccountSuccess":
            MessageLookupByLibrary.simpleMessage("Criar outra conta"),
        "createAccountText":
            MessageLookupByLibrary.simpleMessage("Crie sua primeira conta"),
        "currency": MessageLookupByLibrary.simpleMessage("Moeda"),
        "customColor": MessageLookupByLibrary.simpleMessage("Cor customizada"),
        "customRange":
            MessageLookupByLibrary.simpleMessage("Intervalo de datas"),
        "daily": MessageLookupByLibrary.simpleMessage("Diariamente"),
        "data": MessageLookupByLibrary.simpleMessage("Dados"),
        "date": MessageLookupByLibrary.simpleMessage("Data"),
        "debts": MessageLookupByLibrary.simpleMessage("Dívida"),
        "defaultAccount":
            MessageLookupByLibrary.simpleMessage("Conta por defeito"),
        "delete": MessageLookupByLibrary.simpleMessage("Apagar"),
        "deleteAccount":
            MessageLookupByLibrary.simpleMessage("¿Deletar conta?"),
        "deleteAccountDescription": MessageLookupByLibrary.simpleMessage(
            "A conta será eliminado, tem a certeza?"),
        "deleteAccountDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Esta conta tem transações, exclua ou mova-as primeiro"),
        "deleteAccountError": MessageLookupByLibrary.simpleMessage(
            "Não é possível excluir conta"),
        "deleteCategory":
            MessageLookupByLibrary.simpleMessage("¿Deletar categoría?"),
        "deleteCategoryDescription": MessageLookupByLibrary.simpleMessage(
            "Se va a borrar una categoría, ¿estás seguro?"),
        "deleteCategoryError": MessageLookupByLibrary.simpleMessage(
            "Não pode eliminar a categoria"),
        "deleteCategorytDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Esta categoria tem transacções, apagá-las ou movê-las primeiro"),
        "deleteMovement":
            MessageLookupByLibrary.simpleMessage("¿Deletar movimento?"),
        "deleteMovementDescription": MessageLookupByLibrary.simpleMessage(
            "Um movimento será eliminado, tem a certeza?"),
        "description": MessageLookupByLibrary.simpleMessage("Descrição"),
        "discreetMode": MessageLookupByLibrary.simpleMessage("Modo discreto"),
        "discreetModeExplanation": MessageLookupByLibrary.simpleMessage(
            "O modo discreto oculta as quantidades"),
        "dismiss": MessageLookupByLibrary.simpleMessage("Dispensar"),
        "errorParentCategory":
            MessageLookupByLibrary.simpleMessage("Categoría nao válida"),
        "errorText": MessageLookupByLibrary.simpleMessage("Ocorreu um erro"),
        "every": MessageLookupByLibrary.simpleMessage("cada"),
        "execute": MessageLookupByLibrary.simpleMessage("Executar"),
        "executeNow": MessageLookupByLibrary.simpleMessage("Executar ahora"),
        "executeTransaction":
            MessageLookupByLibrary.simpleMessage("Executar transação?"),
        "executeTransactionDescription": MessageLookupByLibrary.simpleMessage(
            "Isso criará uma nova transação com a data de hoje"),
        "expense": MessageLookupByLibrary.simpleMessage("Despesas"),
        "expenses": MessageLookupByLibrary.simpleMessage("Despesas"),
        "expensesAndAnalytics":
            MessageLookupByLibrary.simpleMessage("Despesas e análises"),
        "expensesMonth":
            MessageLookupByLibrary.simpleMessage("Despesas mensais"),
        "exportDb": MessageLookupByLibrary.simpleMessage(
            "Exportação copia de segurança"),
        "exported": MessageLookupByLibrary.simpleMessage("Exportado"),
        "finishText": MessageLookupByLibrary.simpleMessage("TERMINAR"),
        "frequency": MessageLookupByLibrary.simpleMessage("Frequência"),
        "from": MessageLookupByLibrary.simpleMessage("De"),
        "group": MessageLookupByLibrary.simpleMessage("Grupo"),
        "groupby": MessageLookupByLibrary.simpleMessage("Filtros"),
        "importCSV": MessageLookupByLibrary.simpleMessage("Importação de CSV"),
        "importDb": MessageLookupByLibrary.simpleMessage(
            "Importação copia de segurança"),
        "income": MessageLookupByLibrary.simpleMessage("Rendimentos"),
        "incomes": MessageLookupByLibrary.simpleMessage("Rendimentos"),
        "initialAmount":
            MessageLookupByLibrary.simpleMessage("Quantia inicial"),
        "initialAmountError": MessageLookupByLibrary.simpleMessage(
            "Quantia não pode estar vazia"),
        "language": MessageLookupByLibrary.simpleMessage("Língua"),
        "lastMovements":
            MessageLookupByLibrary.simpleMessage("Últimos movimentos"),
        "lockAppInBackGround": MessageLookupByLibrary.simpleMessage(
            "Bloquear aplicativo em segundo plano"),
        "lockScreenPrompt":
            MessageLookupByLibrary.simpleMessage("Login com impressão digital"),
        "monthly": MessageLookupByLibrary.simpleMessage("Mensal"),
        "movementsTitle": MessageLookupByLibrary.simpleMessage("Movimentos"),
        "name": MessageLookupByLibrary.simpleMessage("Nome"),
        "nameError":
            MessageLookupByLibrary.simpleMessage("Nome não pode estar vazia"),
        "netIncome": MessageLookupByLibrary.simpleMessage("Lucro Líquido"),
        "next": MessageLookupByLibrary.simpleMessage("PRÓXIMO"),
        "nextEvent": MessageLookupByLibrary.simpleMessage("Próximo evento o"),
        "noDataLabel": MessageLookupByLibrary.simpleMessage("Sem dados"),
        "ofText": MessageLookupByLibrary.simpleMessage("DE"),
        "on": MessageLookupByLibrary.simpleMessage("el"),
        "parentCategory": MessageLookupByLibrary.simpleMessage("Categoría pai"),
        "pin": MessageLookupByLibrary.simpleMessage("PIN"),
        "pinDoesntMatch":
            MessageLookupByLibrary.simpleMessage("O pin não correspondede"),
        "pinEmpty":
            MessageLookupByLibrary.simpleMessage("O PIN não pode ficar vazio"),
        "pinError": MessageLookupByLibrary.simpleMessage(
            "O PIN deve ser um valor numérico"),
        "pinErrorLenght":
            MessageLookupByLibrary.simpleMessage("O PIN deve ter 4 números"),
        "prev": MessageLookupByLibrary.simpleMessage("ANTERIOR"),
        "recurrences":
            MessageLookupByLibrary.simpleMessage("Transações recorrentes"),
        "repeat": MessageLookupByLibrary.simpleMessage("Repetir..."),
        "restartMoedeiro": MessageLookupByLibrary.simpleMessage(
            "Importado, reinicie o Moedeiro"),
        "save": MessageLookupByLibrary.simpleMessage("Salvar"),
        "search": MessageLookupByLibrary.simpleMessage("Procurar por..."),
        "security": MessageLookupByLibrary.simpleMessage("Segurança"),
        "selectColor": MessageLookupByLibrary.simpleMessage("Selecione a cor"),
        "selectColorShade": MessageLookupByLibrary.simpleMessage(
            "Selecione a tonalidade da cor"),
        "selectCountry":
            MessageLookupByLibrary.simpleMessage("Selecione seu país"),
        "selectCountryDisclaimer": MessageLookupByLibrary.simpleMessage(
            "Precisamos do país para que Moedeiro possa oferecer os dados com o formato regional correto"),
        "selectCurrency":
            MessageLookupByLibrary.simpleMessage("Seleccione a sua moeda"),
        "selectLanguage": MessageLookupByLibrary.simpleMessage(
            "Selecione o idioma do Moedeiro"),
        "setUpMoedeiro": MessageLookupByLibrary.simpleMessage(
            "Vamos configurar sua primeira conta"),
        "settings": MessageLookupByLibrary.simpleMessage("Definições"),
        "show": MessageLookupByLibrary.simpleMessage("Mostrar"),
        "step": MessageLookupByLibrary.simpleMessage("PASSO"),
        "systemDefaultTitle":
            MessageLookupByLibrary.simpleMessage("Padrão do sistema"),
        "theme": MessageLookupByLibrary.simpleMessage("Tema"),
        "time": MessageLookupByLibrary.simpleMessage("Hora"),
        "to": MessageLookupByLibrary.simpleMessage("A"),
        "transaction": MessageLookupByLibrary.simpleMessage("Transacção"),
        "transactionCreated":
            MessageLookupByLibrary.simpleMessage("Transação criada"),
        "transactions": MessageLookupByLibrary.simpleMessage("Transacções"),
        "transactionsTitle": MessageLookupByLibrary.simpleMessage("Movimentos"),
        "transfer": MessageLookupByLibrary.simpleMessage("Transferência"),
        "transferCreated":
            MessageLookupByLibrary.simpleMessage("Transferência criada"),
        "transfers": MessageLookupByLibrary.simpleMessage("Transferências"),
        "useFingerprint": MessageLookupByLibrary.simpleMessage("Usar pegada"),
        "version": MessageLookupByLibrary.simpleMessage("Versão"),
        "warningClosingBottomSheetSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "As alterações que você fez não serão salvas"),
        "warningClosingBottomSheetTitle":
            MessageLookupByLibrary.simpleMessage("¿Descartar alterações?"),
        "weekly": MessageLookupByLibrary.simpleMessage("Semanalmente"),
        "welcomeBack":
            MessageLookupByLibrary.simpleMessage("Bem-vindo de volta"),
        "yearly": MessageLookupByLibrary.simpleMessage("Anualmente")
      };
}
