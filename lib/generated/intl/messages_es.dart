// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "acceptButtonText": MessageLookupByLibrary.simpleMessage("Aceptar"),
        "account": MessageLookupByLibrary.simpleMessage("Cuenta"),
        "accountName":
            MessageLookupByLibrary.simpleMessage("Nombre de la cuenta"),
        "accountSelectError":
            MessageLookupByLibrary.simpleMessage("Selecciona una cuenta"),
        "accountsTitle": MessageLookupByLibrary.simpleMessage("Cuentas"),
        "allSet": MessageLookupByLibrary.simpleMessage("Bienvenido a Moedeiro"),
        "amount": MessageLookupByLibrary.simpleMessage("Importe"),
        "amountError": MessageLookupByLibrary.simpleMessage(
            "La cantidad tiene que ser mayor a 0"),
        "analytics": MessageLookupByLibrary.simpleMessage("Resumen"),
        "appTitle": MessageLookupByLibrary.simpleMessage("Moedeiro"),
        "authenticateLabel": MessageLookupByLibrary.simpleMessage(
            "Inicia sesión para ver los datos"),
        "balance": MessageLookupByLibrary.simpleMessage("Saldo"),
        "budgetsTitle": MessageLookupByLibrary.simpleMessage("Presupuestos"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancelar"),
        "categories": MessageLookupByLibrary.simpleMessage("Categorías"),
        "categoryError":
            MessageLookupByLibrary.simpleMessage("Selecciona una categoría"),
        "categoryTitle": MessageLookupByLibrary.simpleMessage("Categoría"),
        "changePassword":
            MessageLookupByLibrary.simpleMessage("Cambiar contraseña"),
        "common": MessageLookupByLibrary.simpleMessage("Generales"),
        "confirmPin": MessageLookupByLibrary.simpleMessage("Confirma el PIN"),
        "createAccountError": MessageLookupByLibrary.simpleMessage(
            "Es necesario crear una cuenta"),
        "createAccountSuccess":
            MessageLookupByLibrary.simpleMessage("Crear otra cuenta"),
        "createAccountText":
            MessageLookupByLibrary.simpleMessage("Crea tu primera cuenta"),
        "currency": MessageLookupByLibrary.simpleMessage("Moneda"),
        "customColor":
            MessageLookupByLibrary.simpleMessage("Color personalizado"),
        "customRange": MessageLookupByLibrary.simpleMessage("Rango de fechas"),
        "daily": MessageLookupByLibrary.simpleMessage("Diariamente"),
        "data": MessageLookupByLibrary.simpleMessage("Datos"),
        "date": MessageLookupByLibrary.simpleMessage("Fecha"),
        "debts": MessageLookupByLibrary.simpleMessage("Deudas"),
        "defaultAccount":
            MessageLookupByLibrary.simpleMessage("Cuenta por defecto"),
        "delete": MessageLookupByLibrary.simpleMessage("Eliminar"),
        "deleteAccount":
            MessageLookupByLibrary.simpleMessage("¿Borrar cuenta?"),
        "deleteAccountDescription": MessageLookupByLibrary.simpleMessage(
            "Se va a borrar la cuenta, ¿estás seguro?"),
        "deleteAccountDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Esta cuenta tiene transacciones, borrar o moverlas primero"),
        "deleteAccountError": MessageLookupByLibrary.simpleMessage(
            "No se puede borrar la cuenta"),
        "deleteCategory":
            MessageLookupByLibrary.simpleMessage("¿Borrar categoría?"),
        "deleteCategoryDescription": MessageLookupByLibrary.simpleMessage(
            "Se va a borrar una categoría, ¿estás seguro?"),
        "deleteCategoryError": MessageLookupByLibrary.simpleMessage(
            "No se puede borrar la categoría"),
        "deleteCategorytDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Esta categoría tiene transacciones, borrar o moverlas primero"),
        "deleteMovement":
            MessageLookupByLibrary.simpleMessage("¿Borrar movimiento?"),
        "deleteMovementDescription": MessageLookupByLibrary.simpleMessage(
            "Se va a borrar un movimiento, ¿estás seguro?"),
        "description": MessageLookupByLibrary.simpleMessage("Descripción"),
        "discreetMode": MessageLookupByLibrary.simpleMessage("Modo discreto"),
        "discreetModeExplanation": MessageLookupByLibrary.simpleMessage(
            "El modo discreto oculta los importes"),
        "dismiss": MessageLookupByLibrary.simpleMessage("Descartar"),
        "errorParentCategory":
            MessageLookupByLibrary.simpleMessage("Categoría no válida"),
        "errorText":
            MessageLookupByLibrary.simpleMessage("Se ha producido un error"),
        "every": MessageLookupByLibrary.simpleMessage("cada"),
        "execute": MessageLookupByLibrary.simpleMessage("Ejecutar"),
        "executeNow": MessageLookupByLibrary.simpleMessage("Ejecutar ahora"),
        "executeTransaction":
            MessageLookupByLibrary.simpleMessage("¿Ejecutar transacción?"),
        "executeTransactionDescription": MessageLookupByLibrary.simpleMessage(
            "Esto creará una nueva transacción con fecha de hoy"),
        "expense": MessageLookupByLibrary.simpleMessage("Gasto"),
        "expenses": MessageLookupByLibrary.simpleMessage("Gastos"),
        "expensesAndAnalytics":
            MessageLookupByLibrary.simpleMessage("Gastos y análisis"),
        "expensesMonth": MessageLookupByLibrary.simpleMessage("Gastos mes"),
        "exportDb":
            MessageLookupByLibrary.simpleMessage("Exportar copia de seguridad"),
        "exported": MessageLookupByLibrary.simpleMessage("Exportado"),
        "finishText": MessageLookupByLibrary.simpleMessage("TERMINAR"),
        "frequency": MessageLookupByLibrary.simpleMessage("Frecuencia"),
        "from": MessageLookupByLibrary.simpleMessage("De"),
        "group": MessageLookupByLibrary.simpleMessage("Agrupar"),
        "groupby": MessageLookupByLibrary.simpleMessage("Filtros"),
        "importCSV": MessageLookupByLibrary.simpleMessage("Importar desde CSV"),
        "importDb":
            MessageLookupByLibrary.simpleMessage("Importar copia de seguridad"),
        "income": MessageLookupByLibrary.simpleMessage("Ingreso"),
        "incomes": MessageLookupByLibrary.simpleMessage("Ingresos"),
        "initialAmount":
            MessageLookupByLibrary.simpleMessage("Cantidad inicial"),
        "initialAmountError": MessageLookupByLibrary.simpleMessage(
            "La cantidad no puede estar vacía"),
        "language": MessageLookupByLibrary.simpleMessage("Idioma"),
        "lastMovements":
            MessageLookupByLibrary.simpleMessage("Últimos movimientos"),
        "lockAppInBackGround": MessageLookupByLibrary.simpleMessage(
            "Bloquear aplicación en segundo plano"),
        "lockScreenPrompt":
            MessageLookupByLibrary.simpleMessage("Iniciar sesión con huella"),
        "monthly": MessageLookupByLibrary.simpleMessage("Mensualmente"),
        "movementsTitle": MessageLookupByLibrary.simpleMessage("Movimientos"),
        "name": MessageLookupByLibrary.simpleMessage("Nombre"),
        "nameError": MessageLookupByLibrary.simpleMessage(
            "El nombre no puede estar vacío"),
        "netIncome": MessageLookupByLibrary.simpleMessage("Ingresos netos"),
        "next": MessageLookupByLibrary.simpleMessage("SIGUIENTE"),
        "nextEvent": MessageLookupByLibrary.simpleMessage("Próximo evento el"),
        "noDataLabel": MessageLookupByLibrary.simpleMessage("Sin datos"),
        "ofText": MessageLookupByLibrary.simpleMessage("DE"),
        "on": MessageLookupByLibrary.simpleMessage("el"),
        "parentCategory":
            MessageLookupByLibrary.simpleMessage("Categoría padre"),
        "pin": MessageLookupByLibrary.simpleMessage("PIN"),
        "pinDoesntMatch":
            MessageLookupByLibrary.simpleMessage("El pin no coincide"),
        "pinEmpty":
            MessageLookupByLibrary.simpleMessage("El PIN no puede estar vacío"),
        "pinError": MessageLookupByLibrary.simpleMessage(
            "El PIN tiene que ser un valor numérico"),
        "pinErrorLenght": MessageLookupByLibrary.simpleMessage(
            "El PIN tiene que tener 4 números"),
        "prev": MessageLookupByLibrary.simpleMessage("ANTERIOR"),
        "recurrences":
            MessageLookupByLibrary.simpleMessage("Transacciones recurrentes"),
        "repeat": MessageLookupByLibrary.simpleMessage("Repetir..."),
        "restartMoedeiro": MessageLookupByLibrary.simpleMessage(
            "Importado, reinicia Moedeiro"),
        "save": MessageLookupByLibrary.simpleMessage("Guardar"),
        "search": MessageLookupByLibrary.simpleMessage("Buscar.."),
        "security": MessageLookupByLibrary.simpleMessage("Seguridad"),
        "selectColor":
            MessageLookupByLibrary.simpleMessage("Selecciona el color"),
        "selectColorShade":
            MessageLookupByLibrary.simpleMessage("Selecciona tono de color"),
        "selectCountry":
            MessageLookupByLibrary.simpleMessage("Selecciona tu país"),
        "selectCountryDisclaimer": MessageLookupByLibrary.simpleMessage(
            "Necesitamos el país para que Moedeiro pueda ofrecer los datos con el formato regional correcto"),
        "selectCurrency":
            MessageLookupByLibrary.simpleMessage("Seleccione su moneda"),
        "selectLanguage": MessageLookupByLibrary.simpleMessage(
            "Selecciona el idioma de Moedeiro"),
        "setUpMoedeiro": MessageLookupByLibrary.simpleMessage(
            "Configuremos tu primera cuenta"),
        "settings": MessageLookupByLibrary.simpleMessage("Ajustes"),
        "show": MessageLookupByLibrary.simpleMessage("Mostrar"),
        "step": MessageLookupByLibrary.simpleMessage("PASO"),
        "systemDefaultTitle":
            MessageLookupByLibrary.simpleMessage("Por defecto del sistema"),
        "theme": MessageLookupByLibrary.simpleMessage("Tema"),
        "time": MessageLookupByLibrary.simpleMessage("Hora"),
        "to": MessageLookupByLibrary.simpleMessage("A"),
        "transaction": MessageLookupByLibrary.simpleMessage("Transacción"),
        "transactionCreated":
            MessageLookupByLibrary.simpleMessage("Transacción creada"),
        "transactions": MessageLookupByLibrary.simpleMessage("Transacciones"),
        "transactionsTitle":
            MessageLookupByLibrary.simpleMessage("Movimientos"),
        "transfer": MessageLookupByLibrary.simpleMessage("Transferencia"),
        "transferCreated":
            MessageLookupByLibrary.simpleMessage("Transferencia creada"),
        "transfers": MessageLookupByLibrary.simpleMessage("Transferencias"),
        "useFingerprint": MessageLookupByLibrary.simpleMessage("Usar huella"),
        "version": MessageLookupByLibrary.simpleMessage("Versión"),
        "warningClosingBottomSheetSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "No se guardaran los cambios que has hecho"),
        "warningClosingBottomSheetTitle":
            MessageLookupByLibrary.simpleMessage("¿Descartar cambios?"),
        "weekly": MessageLookupByLibrary.simpleMessage("Semanalmente"),
        "welcomeBack":
            MessageLookupByLibrary.simpleMessage("Bienvenido de vuelta"),
        "yearly": MessageLookupByLibrary.simpleMessage("Anualmente")
      };
}
