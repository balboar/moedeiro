// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a gl locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'gl';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "acceptButtonText": MessageLookupByLibrary.simpleMessage("Aceptar"),
        "account": MessageLookupByLibrary.simpleMessage("Contas"),
        "accountName": MessageLookupByLibrary.simpleMessage("Noma da conta"),
        "accountSelectError":
            MessageLookupByLibrary.simpleMessage("Selecciona unha conta"),
        "accountsTitle": MessageLookupByLibrary.simpleMessage("Contas"),
        "allSet": MessageLookupByLibrary.simpleMessage("Benvido a Moedeiro"),
        "amount": MessageLookupByLibrary.simpleMessage("Importe"),
        "amountError": MessageLookupByLibrary.simpleMessage(
            "A cantidade ten que ser superior a 0"),
        "analytics": MessageLookupByLibrary.simpleMessage("Resumen"),
        "appTitle": MessageLookupByLibrary.simpleMessage("Moedeiro"),
        "authenticateLabel": MessageLookupByLibrary.simpleMessage(
            "Inicia sesión para ver os datos"),
        "balance": MessageLookupByLibrary.simpleMessage("Saldo"),
        "budgetsTitle": MessageLookupByLibrary.simpleMessage("Orzamentos"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancelar"),
        "categories": MessageLookupByLibrary.simpleMessage("Categorías"),
        "categoryError":
            MessageLookupByLibrary.simpleMessage("Selecciona unha categoría"),
        "categoryTitle": MessageLookupByLibrary.simpleMessage("Categoría"),
        "changePassword":
            MessageLookupByLibrary.simpleMessage("Cambiar contrasinal"),
        "common": MessageLookupByLibrary.simpleMessage("Xerais"),
        "confirmPin": MessageLookupByLibrary.simpleMessage("Confirma o PIN"),
        "createAccountError": MessageLookupByLibrary.simpleMessage(
            "E necesario crear unha conta"),
        "createAccountSuccess":
            MessageLookupByLibrary.simpleMessage("Crear outra conta"),
        "createAccountText":
            MessageLookupByLibrary.simpleMessage("Crea a tu primera conta"),
        "currency": MessageLookupByLibrary.simpleMessage("Moeda"),
        "customColor":
            MessageLookupByLibrary.simpleMessage("Cor personalizada"),
        "customRange": MessageLookupByLibrary.simpleMessage("Rango de datas"),
        "daily": MessageLookupByLibrary.simpleMessage("Diariamente"),
        "data": MessageLookupByLibrary.simpleMessage("Datos"),
        "date": MessageLookupByLibrary.simpleMessage("Data"),
        "debts": MessageLookupByLibrary.simpleMessage("Débedas"),
        "defaultAccount":
            MessageLookupByLibrary.simpleMessage("Conta por defecto"),
        "delete": MessageLookupByLibrary.simpleMessage("Eliminar"),
        "deleteAccount": MessageLookupByLibrary.simpleMessage("¿Borrar conta?"),
        "deleteAccountDescription": MessageLookupByLibrary.simpleMessage(
            "¿Estás seguro de que queres eliminar esta conta?"),
        "deleteAccountDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Esta conta ten transaccions, borrarlas ou moverlas primero"),
        "deleteAccountError":
            MessageLookupByLibrary.simpleMessage("Non se pode borrar a conta"),
        "deleteCategory":
            MessageLookupByLibrary.simpleMessage("¿Borrar categoría?"),
        "deleteCategoryDescription": MessageLookupByLibrary.simpleMessage(
            "¿Estás seguro de que queres eliminar esta categoría?"),
        "deleteCategoryError": MessageLookupByLibrary.simpleMessage(
            "Non se pode borrar a categoría"),
        "deleteCategorytDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Esta categoría ten transaccions, borrarlas ou moverlas primero"),
        "deleteMovement":
            MessageLookupByLibrary.simpleMessage("¿Borrar movemento?"),
        "deleteMovementDescription": MessageLookupByLibrary.simpleMessage(
            "¿Estás seguro de que queres eliminar este movemento?"),
        "description": MessageLookupByLibrary.simpleMessage("Descripción"),
        "discreetMode": MessageLookupByLibrary.simpleMessage("Modo discreto"),
        "discreetModeExplanation": MessageLookupByLibrary.simpleMessage(
            "O modo discreto oculta os importes"),
        "dismiss": MessageLookupByLibrary.simpleMessage("Descartar"),
        "errorParentCategory":
            MessageLookupByLibrary.simpleMessage("Categoría non válida"),
        "errorText": MessageLookupByLibrary.simpleMessage("Produciuse un erro"),
        "every": MessageLookupByLibrary.simpleMessage("cada"),
        "execute": MessageLookupByLibrary.simpleMessage("Executar"),
        "executeNow": MessageLookupByLibrary.simpleMessage("Executar agora"),
        "executeTransaction":
            MessageLookupByLibrary.simpleMessage("¿Executar transacción?"),
        "executeTransactionDescription": MessageLookupByLibrary.simpleMessage(
            "Isto creará unha nova transacción coa data de hoxe"),
        "expense": MessageLookupByLibrary.simpleMessage("Gasto"),
        "expenses": MessageLookupByLibrary.simpleMessage("Gastos"),
        "expensesAndAnalytics":
            MessageLookupByLibrary.simpleMessage("Gastos e análise"),
        "expensesMonth": MessageLookupByLibrary.simpleMessage("Gastos do mes"),
        "exportDb": MessageLookupByLibrary.simpleMessage(
            "Exportar copia de seguridade"),
        "exported": MessageLookupByLibrary.simpleMessage("Exportado"),
        "finishText": MessageLookupByLibrary.simpleMessage("TERMINAR"),
        "frequency": MessageLookupByLibrary.simpleMessage("Frecuencia"),
        "from": MessageLookupByLibrary.simpleMessage("De"),
        "group": MessageLookupByLibrary.simpleMessage("Agrupar"),
        "groupby": MessageLookupByLibrary.simpleMessage("Filtros"),
        "importCSV": MessageLookupByLibrary.simpleMessage("Importar dende CSV"),
        "importDb": MessageLookupByLibrary.simpleMessage(
            "Importar copia de seguridade"),
        "income": MessageLookupByLibrary.simpleMessage("Ingreso"),
        "incomes": MessageLookupByLibrary.simpleMessage("Ingresos"),
        "initialAmount":
            MessageLookupByLibrary.simpleMessage("Cantidade inicial"),
        "initialAmountError": MessageLookupByLibrary.simpleMessage(
            "A cantidade non pode estar vacía"),
        "language": MessageLookupByLibrary.simpleMessage("Idioma"),
        "lastMovements":
            MessageLookupByLibrary.simpleMessage("Últimos movementos"),
        "lockAppInBackGround": MessageLookupByLibrary.simpleMessage(
            "Bloquear a aplicación cando está en segundo plano"),
        "lockScreenPrompt": MessageLookupByLibrary.simpleMessage(
            "Iniciar sesión coa impresión dixital"),
        "monthly": MessageLookupByLibrary.simpleMessage("Mensualmente"),
        "movementsTitle": MessageLookupByLibrary.simpleMessage("Movementos"),
        "name": MessageLookupByLibrary.simpleMessage("Nome"),
        "nameError":
            MessageLookupByLibrary.simpleMessage("O nome non pode estar vacío"),
        "netIncome": MessageLookupByLibrary.simpleMessage("Ingresos netos"),
        "next": MessageLookupByLibrary.simpleMessage("SEGUINTE"),
        "nextEvent": MessageLookupByLibrary.simpleMessage("Próximo evento o"),
        "noDataLabel": MessageLookupByLibrary.simpleMessage("Sen datos"),
        "ofText": MessageLookupByLibrary.simpleMessage("DE"),
        "on": MessageLookupByLibrary.simpleMessage("o"),
        "parentCategory": MessageLookupByLibrary.simpleMessage("Categoría pai"),
        "pin": MessageLookupByLibrary.simpleMessage("PIN"),
        "pinDoesntMatch":
            MessageLookupByLibrary.simpleMessage("O pin no coincide"),
        "pinEmpty":
            MessageLookupByLibrary.simpleMessage("O PIN non pode estar vacío"),
        "pinError": MessageLookupByLibrary.simpleMessage(
            "O PIN ten que ser un valor numérico"),
        "pinErrorLenght":
            MessageLookupByLibrary.simpleMessage("O PIN ten que ter 4 números"),
        "prev": MessageLookupByLibrary.simpleMessage("ANTERIOR"),
        "recurrences":
            MessageLookupByLibrary.simpleMessage("Transaccions recurrentes"),
        "repeat": MessageLookupByLibrary.simpleMessage("Repetir..."),
        "restartMoedeiro": MessageLookupByLibrary.simpleMessage(
            "Importado, reinicia Moedeiro"),
        "save": MessageLookupByLibrary.simpleMessage("Gardar"),
        "search": MessageLookupByLibrary.simpleMessage("Buscar.."),
        "security": MessageLookupByLibrary.simpleMessage("Seguridade"),
        "selectColor": MessageLookupByLibrary.simpleMessage("Selecciona a cor"),
        "selectColorShade":
            MessageLookupByLibrary.simpleMessage("Selecciona o tono da cor"),
        "selectCountry":
            MessageLookupByLibrary.simpleMessage("Selecciona o teu país"),
        "selectCountryDisclaimer": MessageLookupByLibrary.simpleMessage(
            "Necesitamos o país para que Moedeiro poda ofrecer os datos co formato rexional correcto"),
        "selectCurrency":
            MessageLookupByLibrary.simpleMessage("Seleccione a sua moeda"),
        "selectLanguage": MessageLookupByLibrary.simpleMessage(
            "Selecciona o idioma de Moedeiro"),
        "setUpMoedeiro": MessageLookupByLibrary.simpleMessage(
            "Configuremos a tua primera conta"),
        "settings": MessageLookupByLibrary.simpleMessage("Axustes"),
        "show": MessageLookupByLibrary.simpleMessage("Mostrar"),
        "step": MessageLookupByLibrary.simpleMessage("PASO"),
        "systemDefaultTitle":
            MessageLookupByLibrary.simpleMessage("Por defecto do sistema"),
        "theme": MessageLookupByLibrary.simpleMessage("Tema"),
        "time": MessageLookupByLibrary.simpleMessage("Hora"),
        "to": MessageLookupByLibrary.simpleMessage("A"),
        "transaction": MessageLookupByLibrary.simpleMessage("Transacción"),
        "transactionCreated":
            MessageLookupByLibrary.simpleMessage("Transacción creada"),
        "transactions": MessageLookupByLibrary.simpleMessage("Transaccions"),
        "transactionsTitle": MessageLookupByLibrary.simpleMessage("Movementos"),
        "transfer": MessageLookupByLibrary.simpleMessage("Transferencia"),
        "transferCreated":
            MessageLookupByLibrary.simpleMessage("Transferencia creada"),
        "transfers": MessageLookupByLibrary.simpleMessage("Transferencias"),
        "useFingerprint":
            MessageLookupByLibrary.simpleMessage("Usar impresión dixital"),
        "version": MessageLookupByLibrary.simpleMessage("Versión"),
        "warningClosingBottomSheetSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Non se gardarán os cambios que fixeches"),
        "warningClosingBottomSheetTitle":
            MessageLookupByLibrary.simpleMessage("¿Descartar cambios?"),
        "weekly": MessageLookupByLibrary.simpleMessage("Semanalmente"),
        "welcomeBack": MessageLookupByLibrary.simpleMessage("Benvido de volta"),
        "yearly": MessageLookupByLibrary.simpleMessage("Anualmente")
      };
}
