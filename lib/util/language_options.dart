import 'package:flutter/material.dart';
import '/generated/l10n.dart';

class LanguageOption {
  final String languageCode;
  final String displayName;
  LanguageOption({required this.languageCode, required this.displayName});
}

List<LanguageOption> languageOptions = [
  LanguageOption(
      languageCode: 'system', displayName: 'Usar idioma del sistema'),
  LanguageOption(languageCode: 'en', displayName: 'English'),
  LanguageOption(languageCode: 'es', displayName: 'Español'),
  LanguageOption(languageCode: 'pt', displayName: 'Português'),
  LanguageOption(languageCode: 'gl', displayName: 'Galego'),
];

String getLocaleLabel(BuildContext context, String locale) {
  LanguageOption activeLocale;
  if (locale == 'system') {
    return S.of(context).systemDefaultTitle;
  } else {
    try {
      activeLocale = languageOptions
          .firstWhere((element) => element.languageCode == locale);
    } catch (e) {
      activeLocale = LanguageOption(languageCode: 'en', displayName: 'English');
    }
    return activeLocale.displayName;
  }
}
