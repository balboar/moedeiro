import 'package:flutter/material.dart';

Icon currencyIcon = Icon(Icons.money_rounded);
Icon calendarIcon = Icon(Icons.calendar_today_rounded);
Icon filterListIcon = Icon(Icons.filter_list_rounded);
Icon analyticsIcon = Icon(Icons.bar_chart_rounded, color: Colors.blue);
Icon accountIcon = Icon(Icons.account_balance_wallet_rounded);
Icon calendarMonthIcon = Icon(Icons.calendar_month_rounded);
Icon debtsIcon = Icon(Icons.thumb_down_rounded, color: Colors.pink);
Icon groupByIcon = Icon(Icons.filter_alt_rounded);
Icon groupByOffIcon = Icon(Icons.filter_alt_off_rounded);
Icon helpIcon = Icon(Icons.help_rounded);
Icon forwardIcon = Icon(Icons.arrow_forward_rounded);
Icon clearIcon = Icon(Icons.clear_rounded);
