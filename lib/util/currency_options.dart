import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:moedeiro/models/settings.dart';

String formatCurrency(BuildContext context, double amount) {
  var _discreetMode =
      Provider.of<SettingsModel>(context, listen: false).discreetMode;
  if (_discreetMode)
    return _formatHiddenCurrency(context);
  else
    return _formatVisibleCurrency(context, amount);
}

String _formatVisibleCurrency(BuildContext context, double amount) {
  CurrencyOption locale =
      Provider.of<SettingsModel>(context, listen: false).activeCurrency;
  var format = NumberFormat.simpleCurrency(name: locale.iso4217Code);
  return format.format(amount);
}

String _formatHiddenCurrency(BuildContext context) {
  CurrencyOption locale =
      Provider.of<SettingsModel>(context, listen: false).activeCurrency;
  var format = NumberFormat.simpleCurrency(name: locale.iso4217Code);
  return '${format.positivePrefix}*****${format.positiveSuffix}';
}

class CurrencyOption {
  final String iso4217Code;
  final String displayName;
  final Locale locale;
  CurrencyOption({
    required this.iso4217Code,
    required this.displayName,
    required this.locale,
  });
}

List<CurrencyOption> currencyOptions = [
  CurrencyOption(
      iso4217Code: 'AUD',
      displayName: 'Australian Dollar',
      locale: Locale("en", "AU")),
  CurrencyOption(
      iso4217Code: 'VES',
      displayName: 'Bolivar Venezolano',
      locale: Locale("es", "VE")),
  CurrencyOption(
      iso4217Code: 'CAD',
      displayName: 'Canadian Dollar',
      locale: Locale("en", "CA")),
  CurrencyOption(
      iso4217Code: 'EUR', displayName: 'Euro', locale: Locale("es", "")),
  CurrencyOption(
      iso4217Code: 'NZD',
      displayName: 'New Zeland Dollar',
      locale: Locale("en", "NZ")),
  CurrencyOption(
      iso4217Code: 'ARS',
      displayName: 'Peso Argentino',
      locale: Locale("es", "AR")),
  CurrencyOption(
      iso4217Code: 'CLP',
      displayName: 'Peso Chileno',
      locale: Locale("es", "CL")),
  CurrencyOption(
      iso4217Code: 'MXN',
      displayName: 'Peso Mexicano',
      locale: Locale("es", "MX")),
  CurrencyOption(
      iso4217Code: 'GBP', displayName: 'Pound', locale: Locale("en", "GB")),
  CurrencyOption(
      iso4217Code: 'BRL',
      displayName: 'Real Brasileiro',
      locale: Locale("pt", "BR")),
  CurrencyOption(
      iso4217Code: 'USD', displayName: 'US Dollar', locale: Locale("en", "US")),
];
