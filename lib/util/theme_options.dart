import 'package:flutter/material.dart';

class AppThemeValue {
  final String key;
  final String value;
  AppThemeValue(this.key, this.value);
}

List<AppThemeValue> themeOptionsEng = [
  AppThemeValue('system', 'System default'),
  AppThemeValue('light', 'Light'),
  AppThemeValue('dark', 'Dark'),
];

List<AppThemeValue> themeOptionsEsp = [
  AppThemeValue('system', 'Usar tema del sistema'),
  AppThemeValue('light', 'Claro'),
  AppThemeValue('dark', 'Oscuro')
];

List<AppThemeValue> themeOptionsPT = [
  AppThemeValue('system', 'Usar tema do sistema'),
  AppThemeValue('light', 'Claro'),
  AppThemeValue('dark', 'Escuro')
];

List<AppThemeValue> themeOptions(
  BuildContext context,
) {
  Locale locale = Localizations.localeOf(context);
  switch (locale.languageCode) {
    case 'es':
      return themeOptionsEsp;
    case 'pt':
      return themeOptionsPT;
    default:
      return themeOptionsEng;
  }
}
