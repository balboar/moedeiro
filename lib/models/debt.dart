class Debts {
  static String table = 'debts';
  //  'CREATE TABLE debts (uuid TEXT PRIMARY KEY NOT NULL, description TEXT, amount REAL, ' +
  //           ' category TEXT,account TEXT, timestamp INTEGER, recurrence TEXT, FOREIGN KEY(recurrence) ' +
  //           ' REFERENCES recurrences(uuid),FOREIGN KEY(account) REFERENCES accounts(uuid), ' +
  //           'FOREIGN KEY(category) REFERENCES category(uuid) )',
  //     );

  String? uuid;
  String? description;
  double? amount;
  String? category;
  String? categoryName;
  String? account;
  String? accountName;
  int? timestamp;
  String? recurrence;
  String? periodicity;
  int? periodicityInterval;
  int? nextEvent;
}
