import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:moedeiro/screens/summary/summary.dart';
import 'package:moedeiro/util/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsModel extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.system;
  String _activeTheme = 'system';
  bool _lockScreen = false;
  bool _discreetMode = false;
  bool _useBiometrics = false;
  TransactionTypeFilter _defaultTransactionTypeFilter =
      TransactionTypeFilter.All;
  String _pin = '0000';
  late SharedPreferences prefs;
  String _localeString = '';
  Locale? locale;
  bool _firstTime = true;
  late LanguageOption activeLocale;
  Color _themeColor = Colors.teal;
  Color _defaultThemeColor = Colors.teal;
  bool _useDefaultThemeColor = true;
  CurrencyOption _activeCurrency = CurrencyOption(
      iso4217Code: 'EUR', displayName: 'Euro', locale: Locale("es", ""));

  Future<bool> initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    _discreetMode = prefs.getBool('discreetMode') ?? false;
    _lockScreen = prefs.getBool('lockApp') ?? false;
    _useBiometrics = prefs.getBool('useBiometrics') ?? false;
    _pin = prefs.getString('PIN') ?? '0000';
    _firstTime = prefs.getBool('firstTime') ?? true;

    switch (prefs.getString('defaultTransactionTypeFilter') ?? 'A') {
      case 'A':
        _defaultTransactionTypeFilter = TransactionTypeFilter.All;
        break;
      case 'E':
        _defaultTransactionTypeFilter = TransactionTypeFilter.Expense;
        break;
      case 'I':
        _defaultTransactionTypeFilter = TransactionTypeFilter.Income;
        break;
      default:
        _defaultTransactionTypeFilter = TransactionTypeFilter.All;
    }
    _useDefaultThemeColor = prefs.getBool('useDefaultThemeColor') ?? true;
    _localeString = prefs.getString('locale') ?? 'system';

    _themeColor = Color(prefs.getInt('color') ?? _themeColor.value);
    if (_localeString == 'system') {
      locale = null;
    } else {
      try {
        activeLocale = languageOptions
            .firstWhere((element) => element.languageCode == _localeString);
      } catch (e) {
        activeLocale =
            LanguageOption(languageCode: 'en', displayName: 'English');
      }

      locale = Locale.fromSubtags(languageCode: activeLocale.languageCode);
    }
    var _currency = prefs.getString('currency');
    if (_currency != null)
      _activeCurrency = currencyOptions
          .firstWhere((element) => element.iso4217Code == _currency);
    _activeTheme = prefs.getString('theme') ?? 'system';
    setActiveTheme(_activeTheme);

    return Future.value(true);
  }

  Color get themeColor => _themeColor;
  set themeColor(Color value) {
    _themeColor = value;
    prefs.setInt('color', value.value);
  }

  void setDefaultThemeColor() {
    _themeColor = _defaultThemeColor;
    prefs.setInt('color', _themeColor.value);
  }

  bool get useDefaultThemeColor => _useDefaultThemeColor;
  set useDefaultThemeColor(bool value) {
    _useDefaultThemeColor = value;
    prefs.setBool('useDefaultThemeColor', value);
  }

  bool get firstTime => _firstTime;
  set firstTime(bool value) {
    _firstTime = value;
    notifyListeners();
    prefs.setBool('firstTime', value);
  }

  String get pin => _pin;
  set pin(String value) {
    _pin = value;
    notifyListeners();
    prefs.setString('PIN', _pin);
  }

  void removePin() {
    prefs.remove('PIN');
    _pin = '';
  }

  String get localeString => _localeString;
  set localeString(String value) {
    _localeString = value;

    notifyListeners();
    prefs.setString('locale', _localeString);
  }

  CurrencyOption get activeCurrency => _activeCurrency;
  set activeCurrency(CurrencyOption value) {
    _activeCurrency = value;

    notifyListeners();
    prefs.setString('currency', _activeCurrency.iso4217Code);
  }

  bool get useBiometrics => _useBiometrics;
  set useBiometrics(bool value) {
    _useBiometrics = value;
    notifyListeners();
    prefs.setBool('useBiometrics', _useBiometrics);
  }

  bool get lockScreen => _lockScreen;
  set lockScreen(bool value) {
    _lockScreen = value;
    notifyListeners();
    prefs.setBool('lockApp', _lockScreen);
  }

  TransactionTypeFilter get defaultTransactionTypeFilter =>
      _defaultTransactionTypeFilter;
  set defaultTransactionTypeFilter(TransactionTypeFilter value) {
    _defaultTransactionTypeFilter = value;
    notifyListeners();
    prefs.setString(
        'defaultTransactionTypeFilter', _defaultTransactionTypeFilter.value);
  }

  bool get discreetMode => _discreetMode;
  set discreetMode(bool value) {
    _discreetMode = value;
    notifyListeners();
    prefs.setBool('discreetMode', _discreetMode);
  }

  String get activeTheme => _activeTheme;
  set activeTheme(String value) {
    setActiveTheme(value);
  }

  setSelectedThemeMode(ThemeMode _themeMode) {
    themeMode = _themeMode;
    prefs.setString('theme', _activeTheme.toString());
    notifyListeners();
  }

  void setActiveTheme(String value) {
    _activeTheme = value;
    switch (value) {
      case 'system':
        var brightness = PlatformDispatcher.instance.platformBrightness;
        if (Brightness.dark == brightness)
          themeMode = ThemeMode.dark;
        else
          themeMode = ThemeMode.light;
        break;

      case 'dark':
        themeMode = ThemeMode.dark;
        break;

      default:
        themeMode = ThemeMode.light;
        break;
    }
    notifyListeners();
    prefs.setString('theme', _activeTheme);
  }
}
